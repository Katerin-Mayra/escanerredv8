import os
import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "siprog.settings")
django.setup()
from app.elementos.models import Secretaria, Unidad, Area

def Secretaria_populate():
    Secretaria.objects.get_or_create(nombre="SECRETARÍA DEPARTAMENTAL DE COORDINACIÓN GENERAL", sigla="SDCG", orden=1, tipo="SECRETARÍA", codigo_poa = 1)
    Secretaria.objects.get_or_create(nombre="SECRETARÍA DEPARTAMENTAL DE PLANIFICACIÓN DEL DESARROLLO", sigla="SDPD", orden=2, tipo="SECRETARÍA", codigo_poa = 2)
    Secretaria.objects.get_or_create(nombre="SECRETARÍA DEPARTAMENTAL ADMINISTRATIVA Y FINANCIERO", sigla="SDAF", orden=3, tipo="SECRETARÍA", codigo_poa = 3)
    Secretaria.objects.get_or_create(nombre="SECRETARÍA DEPARTAMENTAL DE DESARROLLO HUMANO", sigla="SDDH", orden=4, tipo="SECRETARÍA", codigo_poa = 4)
    Secretaria.objects.get_or_create(nombre="SECRETARÍA DEPARTAMENTAL DE LA MADRE TIERRA", sigla="SDMT", orden=5, tipo="SECRETARÍA", codigo_poa = 5)
    Secretaria.objects.get_or_create(nombre="SECRETARÍA DEPARTAMENTAL DE OBRAS PÚBLICAS Y SERVICIOS", sigla="SDOPS", orden=6, tipo="SECRETARÍA", codigo_poa = 6)
    Secretaria.objects.get_or_create(nombre="SECRETARÍA DEPARTAMENTAL DE MINERÍA Y METALURGIA", sigla="SDMM", orden=7, tipo="SECRETARÍA", codigo_poa = 7)
    Secretaria.objects.get_or_create(nombre="SECRETARÍA DEPARTAMENTAL DE TURISMO Y CULTURA", sigla="SDTC", orden=8, tipo="SECRETARÍA", codigo_poa = 8)
    Secretaria.objects.get_or_create(nombre="SECRETARÍA DEPARTAMENTAL DE DESARROLLO AGROPECUARIO Y SEGURIDAD ALIMENTARIA", sigla="SDDAySA", orden=9, tipo="SECRETARÍA", codigo_poa = 9)
    Secretaria.objects.get_or_create(nombre="SECRETARÍA DEPARTAMENTAL DE INDUSTRIALIZACIÓN", sigla="SDI", orden=10, tipo="SECRETARÍA", codigo_poa = 10)
    Secretaria.objects.get_or_create(nombre="SECRETARÍA DEPARTAMENTAL JURÍDICA", sigla="SDJ", orden=11, tipo="SECRETARÍA", codigo_poa = 11)

    Secretaria.objects.get_or_create(nombre="PROYECTOS FPS", sigla="FPS", orden=12, tipo="OTROS")
    Secretaria.objects.get_or_create(nombre="PROYECTOS FNDR", sigla="FNDR", orden=13, tipo="OTROS")
    Secretaria.objects.get_or_create(nombre="DESPACHO", sigla="DES", orden=14, tipo="OTROS")

    Secretaria.objects.get_or_create(nombre="DIRECCIÓN DEPARTAMENTAL AUDITORÍA INTERNA", sigla="DDAI", orden=15, tipo="DIRECCIÓN")

def Unidad_populate():
    Unidad.objects.get_or_create(nombre="UNIDAD DE LÍMITES", sigla="LÍMITES", orden=1, secretaria=Secretaria.objects.get(nombre="SECRETARÍA DEPARTAMENTAL DE COORDINACIÓN GENERAL"))

    Unidad.objects.get_or_create(nombre="UNIDAD DE RECURSOS HUMANOS", sigla="RRHH", orden=1, secretaria=Secretaria.objects.get(nombre="SECRETARÍA DEPARTAMENTAL ADMINISTRATIVA Y FINANCIERO"))
    Unidad.objects.get_or_create(nombre="UNIDAD ADMINISTRATIVA", sigla="UADM", orden=2, secretaria=Secretaria.objects.get(nombre="SECRETARÍA DEPARTAMENTAL ADMINISTRATIVA Y FINANCIERO"))
    Unidad.objects.get_or_create(nombre="UNIDAD FINANCIERA", sigla="UFIN", orden=3, secretaria=Secretaria.objects.get(nombre="SECRETARÍA DEPARTAMENTAL ADMINISTRATIVA Y FINANCIERO"))

    Unidad.objects.get_or_create(nombre="SERVICIO DEPARTAMENTAL DE GESTIÓN SOCIAL", sigla="SEDEGES", orden=1, secretaria=Secretaria.objects.get(nombre="SECRETARÍA DEPARTAMENTAL DE DESARROLLO HUMANO"), tipo="SERVICIO")
    Unidad.objects.get_or_create(nombre="SERVICIO DEPARTAMENTAL DE DEPORTES", sigla="SE.DE.DE", orden=2, secretaria=Secretaria.objects.get(nombre="SECRETARÍA DEPARTAMENTAL DE DESARROLLO HUMANO"), tipo="SERVICIO")
    Unidad.objects.get_or_create(nombre="SERVICIO DEPARTAMENTAL DE SALUD", sigla="SEDES", orden=3, secretaria=Secretaria.objects.get(nombre="SECRETARÍA DEPARTAMENTAL DE DESARROLLO HUMANO"), tipo="SERVICIO")
    Unidad.objects.get_or_create(nombre="CODEPEDIS", sigla="CODEPEDIS", orden=4, secretaria=Secretaria.objects.get(nombre="SECRETARÍA DEPARTAMENTAL DE DESARROLLO HUMANO"), tipo="ESPECIAL")

    Unidad.objects.get_or_create(nombre="UNIDAD DE AGUA Y SANEAMIENTO BÁSICO", sigla="UNASBA", orden=1, secretaria=Secretaria.objects.get(nombre="SECRETARÍA DEPARTAMENTAL DE LA MADRE TIERRA"))
    Unidad.objects.get_or_create(nombre="UNIDAD DE GESTIÓN AMBIENTAL Y USO RECURSOS NATURALES", sigla="UGARRNN", orden=2, secretaria=Secretaria.objects.get(nombre="SECRETARÍA DEPARTAMENTAL DE LA MADRE TIERRA"))
    Unidad.objects.get_or_create(nombre="UNIDAD DE GESTIÓN TERRITORIAL Y MANEJO DE CUENCAS", sigla="UGTMIC", orden=3, secretaria=Secretaria.objects.get(nombre="SECRETARÍA DEPARTAMENTAL DE LA MADRE TIERRA"))

    Unidad.objects.get_or_create(nombre="UNIDAD DE ELECTRIFICACIÓN RURAL", sigla="UER", orden=1, secretaria=Secretaria.objects.get(nombre="SECRETARÍA DEPARTAMENTAL DE OBRAS PÚBLICAS Y SERVICIOS"))
    Unidad.objects.get_or_create(nombre="UNIDAD DE OBRAS CIVILES", sigla="CIVIL", orden=2, secretaria=Secretaria.objects.get(nombre="SECRETARÍA DEPARTAMENTAL DE OBRAS PÚBLICAS Y SERVICIOS"))
    Unidad.objects.get_or_create(nombre="UNIDAD DE INFRAESTRUCTURA VIAL", sigla="VIAL", orden=3, secretaria=Secretaria.objects.get(nombre="SECRETARÍA DEPARTAMENTAL DE OBRAS PÚBLICAS Y SERVICIOS"))
    Unidad.objects.get_or_create(nombre="SERVICIO DEPARTAMENTAL DE CAMINOS", sigla="SEDECA", orden=4, secretaria=Secretaria.objects.get(nombre="SECRETARÍA DEPARTAMENTAL DE OBRAS PÚBLICAS Y SERVICIOS"), tipo="SERVICIO")
    Unidad.objects.get_or_create(nombre="UNIDAD DE INFRAESTRUCTURA EDUCATIVA", sigla="EDUC", orden=5, secretaria=Secretaria.objects.get(nombre="SECRETARÍA DEPARTAMENTAL DE OBRAS PÚBLICAS Y SERVICIOS"), tipo="ANTIGUO")
    Unidad.objects.get_or_create(nombre="OTROS INFRAESTRUCTURA", sigla="OINF", orden=6, secretaria=Secretaria.objects.get(nombre="SECRETARÍA DEPARTAMENTAL DE OBRAS PÚBLICAS Y SERVICIOS"), tipo="ANTIGUO")

    Unidad.objects.get_or_create(nombre="UNIDAD MINERA METALÚRGICA", sigla="UMM", orden=1, secretaria=Secretaria.objects.get(nombre="SECRETARÍA DEPARTAMENTAL DE MINERÍA Y METALURGIA"))
    Unidad.objects.get_or_create(nombre="UND. PLANIFICACIÓN SEG. PROSP. EXPLORACIÓN", sigla="UPSPE", orden=2, secretaria=Secretaria.objects.get(nombre="SECRETARÍA DEPARTAMENTAL DE MINERÍA Y METALURGIA"))
    Unidad.objects.get_or_create(nombre="RECAUDACIÓN PERCEPCION CONTROL REGALÍAS MINERAS", sigla="RPCRM", orden=3, secretaria=Secretaria.objects.get(nombre="SECRETARÍA DEPARTAMENTAL DE MINERÍA Y METALURGIA"))

    Unidad.objects.get_or_create(nombre="UNIDAD DE CULTURAS", sigla="CULTURAS", orden=1, secretaria=Secretaria.objects.get(nombre="SECRETARÍA DEPARTAMENTAL DE TURISMO Y CULTURA"))
    Unidad.objects.get_or_create(nombre="UNIDAD DE TURISMO", sigla="TURISMO", orden=2, secretaria=Secretaria.objects.get(nombre="SECRETARÍA DEPARTAMENTAL DE TURISMO Y CULTURA"))

    Unidad.objects.get_or_create(nombre="UNIDAD DE RIEGO", sigla="RIEGO", orden=1, secretaria=Secretaria.objects.get(nombre="SECRETARÍA DEPARTAMENTAL DE DESARROLLO AGROPECUARIO Y SEGURIDAD ALIMENTARIA"))
    Unidad.objects.get_or_create(nombre="SERVICIO DEPARTAMENTAL AGROPECUARIO SEDAG", sigla="SEDAG", orden=2, secretaria=Secretaria.objects.get(nombre="SECRETARÍA DEPARTAMENTAL DE DESARROLLO AGROPECUARIO Y SEGURIDAD ALIMENTARIA"), tipo="SERVICIO")
    
    Unidad.objects.get_or_create(nombre="DIRECCIÓN DE INDUSTRIALIZACIÓN", sigla="INDUST.", orden=1, secretaria=Secretaria.objects.get(nombre="SECRETARÍA DEPARTAMENTAL DE INDUSTRIALIZACIÓN"), tipo="ANTIGUO")
    Unidad.objects.get_or_create(nombre="DIRECCIÓN DE EMPRENDIMIENTOS EMPRESARIALES", sigla="EMPR.", orden=2, secretaria=Secretaria.objects.get(nombre="SECRETARÍA DEPARTAMENTAL DE INDUSTRIALIZACIÓN"), tipo="ANTIGUO")

    Unidad.objects.get_or_create(nombre="DESARROLLO AGROPECUARIO Y SEGURIDAD ALIMENTARIA", sigla="FPS AG.", orden=1, secretaria=Secretaria.objects.get(nombre="PROYECTOS FPS"))
    
    Unidad.objects.get_or_create(nombre="GABINETE DE DESPACHO DE GOBERNACIÓN", sigla="GABINETE", orden=1, secretaria=Secretaria.objects.get(nombre="DESPACHO"))
    Unidad.objects.get_or_create(nombre="COMUNICACIÓN SOCIAL", sigla="COM", orden=2, secretaria=Secretaria.objects.get(nombre="DESPACHO"))
    Unidad.objects.get_or_create(nombre="SEGURIDAD CIUDADANA", sigla="SC", orden=3, secretaria=Secretaria.objects.get(nombre="DESPACHO"), tipo="SERVICIO")
    Unidad.objects.get_or_create(nombre="ASOSORÍA ESTRATÉGICA", sigla="AE", orden=4, secretaria=Secretaria.objects.get(nombre="DESPACHO"), tipo="ESPECIAL")

    Unidad.objects.get_or_create(nombre="JEFATURA DE UNIDAD JURÍDICA", sigla="JUJ", orden=1, secretaria=Secretaria.objects.get(nombre="SECRETARÍA DEPARTAMENTAL JURÍDICA"))

def Area_populate():
    Area.objects.get_or_create(nombre="ÁREA DE PROGRAMACIÓN DE OPERACIONES Y SEGUIMIENTO", sigla="POYS", orden=1, secretaria=Secretaria.objects.get(nombre="SECRETARÍA DEPARTAMENTAL DE PLANIFICACIÓN DEL DESARROLLO"))
    Area.objects.get_or_create(nombre="ÁREA DE DESARROLLO ORGANIZACIONAL", sigla="ADO", orden=2, secretaria=Secretaria.objects.get(nombre="SECRETARÍA DEPARTAMENTAL DE PLANIFICACIÓN DEL DESARROLLO"))
    Area.objects.get_or_create(nombre="ÁREA DE PLANIFICACIÓN DE PROYECTOS", sigla="APP", orden=3, secretaria=Secretaria.objects.get(nombre="SECRETARÍA DEPARTAMENTAL DE PLANIFICACIÓN DEL DESARROLLO"))

    Area.objects.get_or_create(nombre="CONTRATACIONES", sigla="CONTR.", orden=1, unidad=Unidad.objects.get(nombre="UNIDAD ADMINISTRATIVA"))
    Area.objects.get_or_create(nombre="BIENES Y SERVICIOS", sigla="BYS", orden=2, unidad=Unidad.objects.get(nombre="UNIDAD ADMINISTRATIVA"))
    Area.objects.get_or_create(nombre="ACTIVOS FIJOS", sigla="AF", orden=3, unidad=Unidad.objects.get(nombre="UNIDAD ADMINISTRATIVA"))
    Area.objects.get_or_create(nombre="ALMACENES", sigla="ALM", orden=4, unidad=Unidad.objects.get(nombre="UNIDAD ADMINISTRATIVA"), tipo="DÉVIL")
    Area.objects.get_or_create(nombre="ARCHIVOS", sigla="ARCH", orden=5, unidad=Unidad.objects.get(nombre="UNIDAD ADMINISTRATIVA"), tipo="DÉVIL")
    Area.objects.get_or_create(nombre="MANTENIMIENTO DE BIENES INMUEBLES", sigla="MANT", orden=6, unidad=Unidad.objects.get(nombre="UNIDAD ADMINISTRATIVA"), tipo="DÉVIL")
    Area.objects.get_or_create(nombre="MAESTRANZA", sigla="MAEZ", orden=7, unidad=Unidad.objects.get(nombre="UNIDAD ADMINISTRATIVA"), tipo="DÉVIL")

    Area.objects.get_or_create(nombre="SISTEMAS", sigla="SIS", orden=1, unidad=Unidad.objects.get(nombre="UNIDAD FINANCIERA"))
    Area.objects.get_or_create(nombre="PRESUPUESTOS", sigla="PRE", orden=2, unidad=Unidad.objects.get(nombre="UNIDAD FINANCIERA"))
    Area.objects.get_or_create(nombre="TESORERÍA", sigla="TES", orden=3, unidad=Unidad.objects.get(nombre="UNIDAD FINANCIERA"))
    Area.objects.get_or_create(nombre="CONTABILIDAD", sigla="CON", orden=4, unidad=Unidad.objects.get(nombre="UNIDAD FINANCIERA"))

    Area.objects.get_or_create(nombre="ÁREA SOCIAL GÉNERO", sigla="ASG", orden=1, secretaria=Secretaria.objects.get(nombre="SECRETARÍA DEPARTAMENTAL DE DESARROLLO HUMANO"))

    Area.objects.get_or_create(nombre="ÁREA DE GESTIÓN DE RIESGOS", sigla="AGR", orden=1, secretaria=Secretaria.objects.get(nombre="SECRETARÍA DEPARTAMENTAL DE DESARROLLO AGROPECUARIO Y SEGURIDAD ALIMENTARIA"))

    for object in Area.objects.all():
        if object.secretaria is None and object.unidad is not None:
            object.secretaria = object.unidad.secretaria
            object.save()

if __name__ == '__main__':
    Secretaria_populate()
    Unidad_populate()
    Area_populate()
