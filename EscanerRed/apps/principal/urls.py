from django.contrib import admin
from django.urls import path,include
from apps.principal.views import index,direccion_view, direccion_list,direccion_edit,direccion_delete,DireccionList,DireccionCreate,DirecionUpdate,DireccionDelete,EquipoCreate,EquipoDelete,EquipoList,EquipoUpdate,RelacionCreate,RelacionDelete,RelacionFormulario,RelacionList,RelacionUpdate,PerfilList,PerfilCreate,PerfilUpdate,PerfilDelete,RelacionPerfilCreate,RelacionPerfilDelete,RelacionPerfilList,RelacionPerfilUpdate
from apps.principal.views import PerfilDetail,PerfilEquipoDetail,PerfilDetailAll,REDCreate,REdList,RedDetail,REDUpdate,REDDelete,IP_RelacionCreate,Equipo_ipRelacionCreate,PerfilListall,PerfilDetail,PerfilDetailno,REDCreateDos,Equipo_ipRelacionCreateDos
from apps.principal.views import InfraestructuraList,InfraestructuraCreate,InfraestructuraUpdate,InfraestructuraDelete,InfraestructuraDetail,RelacionPerfilCreateDos
from apps.principal.views import SecretariaList,SecretariaCreate,SecretariaCreateDos,SecretariaUpdate,SecretariaDelete,SecretariaDetail
from apps.principal.views import SecretariaAreaList,SecretariaAreaCreate,SecretariaAreaCreateDos,SecretariaAreaUpdate,SecretariaAreaDelete,SecretariaAreaDetail

from apps.principal.views import SecretariavList,SecretariavCreate,SecretariavCreateDos,SecretariavUpdate,SecretariavDelete,SecretariavDetail
from apps.principal.views import UnidadvList,UnidadvCreate,UnidadvCreateDos,UnidadvUpdate,UnidadvDelete,UnidadvDetail,UnidadvDetailView
from apps.principal.views import AreavList,AreavCreate,AreavCreateDos,AreavUpdate,AreavDelete,AreavDetail
from apps.principal.views import AreavUniCreate,AreavUniCreateDos

from apps.principal.views import AreaList,AreaCreate,AreaUpdate,AreaDelete,AreaDetail
from apps.principal.views import UnidadList,UnidadCreate,UnidadCreateDos,UnidadUpdate,UnidadDelete,UnidadDetail,UnidadvDeletedos

from apps.principal.views import InfraestructuraSecretariaList,InfraestructuraSecretariaCreate,InfraestructuraSecretariaUpdate,InfraestructuraSecretariaDelete,InfraestructuraSecretariaDetail,InfraestructuraSecretariaCreatedos
from apps.principal.views import InfraestructuraUnidadList,InfraestructuraUnidadCreate,InfraestructuraUnidadCreateDos,InfraestructuraUnidadUpdate,InfraestructuraUnidadDelete,InfraestructuraUnidadDetail

from apps.principal.views import InfraestructuraAreaList,InfraestructuraAreaCreate,InfraestructuraAreaUpdate,InfraestructuraAreaDelete,InfraestructuraUnidadDetail

urlpatterns = [
    path('',index, name='index'),
    
    #URL con funciones
    #URL con clases
    #Direccion
    path('direccion/listar/',DireccionList.as_view(), name='direccion_listar'),
    path('direccion/crear/',DireccionCreate.as_view(), name='direccion_crear'),
    
    path('direccion/editar/<pk>/',DirecionUpdate.as_view(), name='direccion_edit'),
    path('direccion/eliminar/<pk>/',DireccionDelete.as_view(), name='direccion_delete'),

    

    #Equipo

    path('equipo/listar/',EquipoList.as_view(), name='equipo_listar'),
    path('equipo/crear/',EquipoCreate.as_view(), name='equipo_crear'),
    
    path('equipo/editar/<pk>/',EquipoUpdate.as_view(), name='equipo_edit'),
    path('equipo/eliminar/<pk>/',EquipoDelete.as_view(), name='equipo_delete'),

    #Relacion

    path('relacion/listar/',RelacionList.as_view(), name='relacion_listar'),
    path('relacion/crear/',RelacionCreate.as_view(), name='relacion_crear'),
    
    path('relacion/editar/<pk>/',RelacionUpdate.as_view(), name='relacion_edit'),
    path('relacion/eliminar/<pk>/',RelacionDelete.as_view(), name='relacion_delete'),


    path('ip_relacion/crear/<pk>/',IP_RelacionCreate.as_view(), name='ip_relacion_crear'),
    path('equipo_ip_relacion/crear/<pk>/',Equipo_ipRelacionCreate.as_view(), name='equipo_ip_relacion_crear'),
    path('equipo_ip_relaciondos/crear/<pk>/',Equipo_ipRelacionCreateDos.as_view(), name='equipo_ip_relacion_crear_dos'),

    #Perfil

    path('perfil/listar/',PerfilList.as_view(), name='perfil_listar'),
    path('perfil/crear/',PerfilCreate.as_view(), name='perfil_crear'),
    
    path('perfil/editar/<pk>/',PerfilUpdate.as_view(), name='perfil_edit'),
    path('perfil/eliminar/<pk>/',PerfilDelete.as_view(), name='perfil_delete'),

    path('perfilall/listar/',PerfilListall.as_view(), name='perfil_listar_all'),

    path('perfildetail/listar/<int:pk>/',PerfilDetail.as_view(), name='perfil_detail_list'),


    #RelacionPerfil
    
    path('Relacionperfil/listar/',RelacionPerfilList.as_view(), name='relacionPerfil_listar'),
    
    path('Relacionperfil/crear/<pk>/',RelacionPerfilCreate.as_view(), name='relacionPerfil_crear'),

    path('Relacionperfildos/crear/<pk>/',RelacionPerfilCreateDos.as_view(), name='relacionPerfil_crear_dos'),
    
    path('Relacionperfil/editar/<pk>/',RelacionPerfilUpdate.as_view(), name='relacionPerfil_edit'),

    path('Relacionperfil/eliminar/<pk>/',RelacionPerfilDelete.as_view(), name='relacionPerfil_delete'),

    #RelacionPerfilUsuario
    path('PerfilEquipoDetail/listar/<int:pk>/',PerfilEquipoDetail.as_view(), name='PerfilEquipoDetail_listar'),
    path('PerfilDetalle/listar/<int:pk>/',PerfilDetailno.as_view(), name='perfildetail_listar'),  
    

       #Mostrar Todos los usuarios y los equipos
    path('PerfilEquipoDetailAll/listar/',PerfilDetailAll.as_view(), name='PerfilDetailAll_listar'),

    
    
    #Mostrar las Redes
    path('red/crear/',REDCreate.as_view(), name='red_crear'),
     path('reddos/crear/',REDCreateDos.as_view(), name='red_creardos'),
    path('red/listar/',REdList.as_view(), name='red_listar'),
    path('red/editar/<pk>/',REDUpdate.as_view(), name='redes_edit'),
    path('red/eliminar/<pk>/',REDDelete.as_view(), name='redes_delete'),


    #Mostrar las direcciones por Red
     path('reddetail/listar/<int:pk>/',RedDetail.as_view(), name='reddetail_listar'),
     
 
    #Infraestructura

    path('infraestructura/listar/',InfraestructuraList.as_view(), name='infraestructura_listar'),
    path('infraestructura/crear/',InfraestructuraCreate.as_view(), name='infraestructura_crear'),
    
    path('infraestructura/editar/<pk>/',InfraestructuraUpdate.as_view(), name='infraestructura_edit'),
    path('infraestructura/eliminar/<pk>/',InfraestructuraDelete.as_view(), name='infraestructura_delete'),

    path('infraestructuradetail/listar/<int:pk>/',InfraestructuraDetail.as_view(), name='infraestructura_detail_list'),

    #secretaria

    path('secretaria/listar/',SecretariaList.as_view(), name='secretaria_listar'),
    path('secretaria/crear/',SecretariaCreate.as_view(), name='secretaria_crear'),

    path('secretariados/crear/',SecretariaCreateDos.as_view(), name='secretaria_crear_dos'),
    
    path('secretaria/editar/<pk>/',SecretariaUpdate.as_view(), name='secretaria_edit'),
    path('secretaria/eliminar/<pk>/',SecretariaDelete.as_view(), name='secretaria_delete'),

    path('secretariadetail/listar/<int:pk>/',SecretariaDetail.as_view(), name='secretaria_detail_list'),


    #unidad

    path('unidad/listar/',UnidadList.as_view(), name='unidad_listar'),
    path('unidad/crear/',UnidadCreate.as_view(), name='unidad_crear'),

    path('unidaddos/crear/',UnidadCreate.as_view(), name='unidad_crear_crear'),
    
    path('unidad/editar/<pk>/',UnidadUpdate.as_view(), name='unidad_edit'),
    path('unidad/eliminar/<pk>/',UnidadDelete.as_view(), name='unidad_delete'),

    path('unidaddetail/listar/<int:pk>/',UnidadDetail.as_view(), name='unidad_detail_list'),

    #area

    path('area/listar/',AreaList.as_view(), name='area_listar'),
    path('area/crear/',AreaCreate.as_view(), name='area_crear'),
    
    path('area/editar/<pk>/',AreaUpdate.as_view(), name='area_edit'),
    path('area/eliminar/<pk>/',AreaDelete.as_view(), name='area_delete'),

    path('areadetail/listar/<int:pk>/',AreaDetail.as_view(), name='area_detail_list'),



    #secretaria area

    path('secretariaarea/listar/',SecretariaAreaList.as_view(), name='secretariaarea_listar'),
    path('secretariaarea/crear/',SecretariaAreaCreate.as_view(), name='secretariaarea_crear'),

    path('secretariaareados/crear/',SecretariaAreaCreateDos.as_view(), name='secretariaarea_crear_dos'),
    
    path('secretariaarea/editar/<pk>/',SecretariaAreaUpdate.as_view(), name='secretariaarea_edit'),
    path('secretariaarea/eliminar/<pk>/',SecretariaAreaDelete.as_view(), name='secretariaarea_delete'),

    path('secretariaareadetail/listar/<int:pk>/',SecretariaAreaDetail.as_view(), name='secretariaarea_detail_list'),





    #secretaria verdadero

    path('secretariav/listar/',SecretariavList.as_view(), name='secretariav_listar'),
    path('secretariav/crear/',SecretariavCreate.as_view(), name='secretariav_crear'),

    path('secretariavdos/crear/',SecretariavCreateDos.as_view(), name='secretariav_crear_dos'),
    
    path('secretariav/editar/<pk>/',SecretariavUpdate.as_view(), name='secretariav_edit'),
    path('secretariav/eliminar/<pk>/',SecretariavDelete.as_view(), name='secretariav_delete'),

    path('secretariavdetail/listar/<int:pk>/',SecretariavDetail.as_view(), name='secretariav_detail_list'),



    #unidad verdadero

    path('unidadv/listar/',UnidadvList.as_view(), name='unidadv_listar'),
    path('unidadv/crear/<int:pk>/',UnidadvCreate.as_view(), name='unidadv_crear'),

    path('unidadvdos/crear/<int:pk>/',UnidadvCreateDos.as_view(), name='unidadv_crear_dos'),
    
    path('unidadv/editar/<pk>/',UnidadvUpdate.as_view(), name='unidadv_edit'),
    path('unidadv/eliminar/<pk>/',UnidadvDelete.as_view(), name='unidadv_delete'),
    path('unidadvdos/eliminar/<pk>/',UnidadvDeletedos.as_view(), name='unidadv_delete_dos'),

    path('unidadvdetail/listar/<int:pk>/',UnidadvDetail.as_view(), name='unidadv_detail_list'),
    path('unidadvdetailview/listar/<int:pk>/',UnidadvDetailView.as_view(), name='unidadv_detail_list_view'),


    #area verdadero

    path('areav/listar/<int:pk>/',AreavList.as_view(), name='areav_listar'),

    path('areav/crear/<int:pk>/',AreavCreate.as_view(), name='areav_crear'),
    path('areavdos/crear/<int:pk>/',AreavCreateDos.as_view(), name='areav_crear_dos'),

    path('areavuni/crear/<int:pk>/',AreavUniCreate.as_view(), name='areavuni_crear'),
    path('areavunidos/crear/<int:pk>/',AreavUniCreateDos.as_view(), name='areavunidos_crear_dos'),
    
    path('areav/editar/<pk>/',AreavUpdate.as_view(), name='areav_edit'),
    path('areav/eliminar/<pk>/',AreavDelete.as_view(), name='areav_delete'),

    path('areavdetail/listar/<int:pk>/',AreavDetail.as_view(), name='areav_detail_list'),

















    #Infraestructura-Secretaria

    path('infraestructuraSecretaria/listar/',InfraestructuraSecretariaList.as_view(), name='infraestructuraSecretaria_listar'),
    path('infraestructuraSecretaria/crear/<pk>/',InfraestructuraSecretariaCreate.as_view(), name='infraestructuraSecretaria_crear'),

    path('infraestructuraSecretariados/crear/<pk>/',InfraestructuraSecretariaCreatedos.as_view(), name='infraestructuraSecretaria_crear_dos'),

    path('infraestructuraSecretaria/editar/<pk>/',InfraestructuraSecretariaUpdate.as_view(), name='infraestructuraSecretaria_edit'),
    path('infraestructuraSecretaria/eliminar/<pk>/',InfraestructuraSecretariaDelete.as_view(), name='infraestructuraSecretaria_delete'),

    path('infraestructuraSecretariadetail/listar/<int:pk>/',InfraestructuraSecretariaDetail.as_view(), name='infraestructuraSecretaria_detail_list'),

    #InfraestructuraUnidad

    path('infraestructuraUnidad/listar/',InfraestructuraUnidadList.as_view(), name='infraestructuraUnidad_listar'),
    path('infraestructuraUnidad/crear/<pk>/',InfraestructuraUnidadCreate.as_view(), name='infraestructuraUnidad_crear'),

    path('infraestructuraUnidaddos/crear/<pk>/',InfraestructuraUnidadCreateDos.as_view(), name='infraestructuraUnidad_crear_dos'),
    
    path('infraestructuraUnidad/editar/<pk>/',InfraestructuraUnidadUpdate.as_view(), name='infraestructuraUnidad_edit'),
    path('infraestructuraUnidad/eliminar/<pk>/',InfraestructuraUnidadDelete.as_view(), name='infraestructuraUnidad_delete'),

    path('infraestructuraUnidaddetail/listar/<int:pk>/',InfraestructuraUnidadDetail.as_view(), name='infraestructuraUnidad_detail_list'),

    #Infraestructura Area

    path('infraestructuraArea/listar/',InfraestructuraAreaList.as_view(), name='infraestructuraArea_listar'),
    path('infraestructuraArea/crear/',InfraestructuraAreaCreate.as_view(), name='infraestructuraArea_crear'),
    
    path('infraestructuraArea/editar/<pk>/',InfraestructuraAreaUpdate.as_view(), name='infraestructuraArea_edit'),
    path('infraestructuraArea/eliminar/<pk>/',InfraestructuraAreaDelete.as_view(), name='infraestructuraArea_delete'),

    path('infraestructuraAreadetail/listar/<int:pk>/',InfraestructuraUnidadDetail.as_view(), name='infraestructuraArea_detail_list'),

]