from django.db import models
from django.conf import settings
from django.utils import timezone
from django.urls import reverse
from macaddress.fields import MACAddressField
# Create your models here.

#  Creacion de modelo para aplicacion de conexion de Redes




#Class Grupo(models.Model):

#...............................................................Secretaria Area Unidad..........................................

class GeneralModel(models.Model):
    esta_activo = models.BooleanField(default = True)
    creado_en = models.DateTimeField(auto_now_add = True, null = True)
    modificado_en = models.DateTimeField(auto_now = True, null = True)
    detalle=models.CharField(max_length = 228, blank = True,default = "Ningun detalle")
    orden = models.IntegerField(default = 0)
    
    class Meta:
        abstract = True

class Secretaria(GeneralModel):
    nombre = models.CharField(max_length = 128)
    sigla = models.CharField(max_length = 16, unique = True)
    tipo = models.CharField(max_length = 16)
    etiqueta = models.CharField(max_length = 128, null = True, blank = True)

    def __str__(self):
        return '{}'.format(self.nombre)

    class Meta:
        verbose_name = 'Secretaría'

class Unidad(GeneralModel):
    nombre = models.CharField(max_length = 128)
    sigla = models.CharField(max_length = 16, unique = True)
    tipo = models.CharField(max_length = 16)
    etiqueta = models.CharField(max_length = 128, null = True, blank = True)
    secretaria = models.ForeignKey(Secretaria, null = True, related_name = 'unidades', on_delete = models.SET_NULL)
    
    def __str__(self):
        return '{}'.format(self.nombre)
    class Meta:
        verbose_name = 'Unidades'

class Area(GeneralModel):
    nombre = models.CharField(max_length = 128)
    sigla = models.CharField(max_length = 16, unique = True)
    tipo = models.CharField(max_length = 16, default = "ÁREA")
    etiqueta = models.CharField(max_length = 128, null = True, blank = True)
    secretaria = models.ForeignKey(Secretaria, null = True, blank = True, related_name = 'areas', on_delete = models.SET_NULL)
    unidad = models.ForeignKey(Unidad, null = True, blank = True, related_name = 'areas', on_delete = models.SET_NULL)

    def __str__(self):
        return '{}'.format(self.nombre)

    class Meta:
        verbose_name = 'Área'




#...............................................................  Redes  .........................................



class Red(models.Model):
    numero = models.CharField(null=True,db_index=True,max_length=167)
    puerta_enlace=models.CharField(max_length=33)
    nombre=models.CharField(max_length=45)
    mascara=models.CharField(max_length=33,default="255.255.255.0")
    inicio= models.IntegerField(null=True,default=0)
    fin=models.IntegerField(null=True,default=255)
    fecha = models.DateField(auto_now_add=True,auto_now=False,blank=True)

    secretaria=models.ManyToManyField(Secretaria,through='RedSecretaria')
    unidad=models.ManyToManyField(Unidad,through='RedUnidad')
    area=models.ManyToManyField(Area,through='RedArea')

    def generarIps(self):
        numero= self.numero.split(".")
        primer=numero[0]
        segundo=numero[1]
        tercero=numero[2]
        cuarto=numero[3]
        print(numero)
        inicio1=self.inicio
       
        fin1=self.fin
        for cuarto in range(inicio1,fin1):
            generado= str(primer)+"."+str(segundo)+"."+str(tercero)+"."+str(cuarto)
            dir = Direccion(
                ip = generado,
                puerta_enlace=self.puerta_enlace,
                mascara = self.mascara,
                red = self
            )
            dir.save()


#................................................. Relacion RED =  Secretaria Area Unidad .........................



class RedSecretaria(models.Model):
    secretaria = models.ForeignKey(Secretaria, null=True, blank=True, on_delete=models.CASCADE)
    red = models.ForeignKey(Red, null=True, blank=True, on_delete=models.CASCADE)
    esta_vigente=models.BooleanField(default=True)
    FechaInicio=models.DateField(auto_now_add=True,auto_now=False,blank=True)
    FechaFinal=models.DateField(blank=True,null=True)

class RedArea(models.Model):
    area = models.ForeignKey(Area, null=True, on_delete=models.CASCADE, blank=True)
    red = models.ForeignKey(Red, null=True, blank=True, on_delete=models.CASCADE)
    esta_vigente=models.BooleanField(default=True)
    FechaInicio=models.DateField(auto_now_add=True,auto_now=False,blank=True)
    FechaFinal=models.DateField(blank=True, null=True)

class RedUnidad(models.Model):
    unidad = models.ForeignKey(Unidad, null=True, on_delete=models.CASCADE, blank=True)
    red = models.ForeignKey(Red, null=True, blank=True, on_delete=models.CASCADE)
    esta_vigente=models.BooleanField(default=True)
    FechaInicio=models.DateField(auto_now_add=True,auto_now=False,blank=True)
    FechaFinal=models.DateField(blank=True, null=True)



 #...................................................................Direccion .................................................
 #    
class Direccion(models.Model):
  
    ip = models.CharField(max_length=33, null=True, verbose_name='IP Address',unique=True,db_index=True)
    puerta_enlace=models.CharField(max_length=33,default="0.0.0.0")
    mascara=models.CharField(max_length=33,default="0.0.0.0")
    red=models.ForeignKey(Red, null=True, on_delete=models.CASCADE, blank=True)
    def get_equipo_vigente(self):
        
        relacion= self.relacion_set.all().filter(esta_vigente=True).last()
        if relacion:
            return relacion.equipo
        else:
            return None

    #ip = models.GenericIPAddressField(db_index=True, null=True, verbose_name='IP Address',unique=True)
    #puerta_enlace=models.GenericIPAddressField(default="192.168.9.1")
    #mascara=models.GenericIPAddressField(default="255.255.255.0")

    def __str__(self):
        return '{}'.format(self.ip)

    def siguente_ip():
        return ""
    
 #.......................................................................Perfil .............................................................

class Perfil(models.Model):
    
    #tipo de datos 
    nombre = models.CharField(max_length=50)
    ci = models.CharField(max_length=10,unique=True)
    apellido = models.CharField(max_length=50)
    correo = models.CharField(max_length=50,unique=True,blank=True)
    tipo=models.CharField(max_length=50,default="Cliente",blank=True)
    fecha = models.DateField(auto_now_add=True,auto_now=False,blank=True)

    secretaria=models.ManyToManyField(Secretaria,through='PefilSecretaria')
    unidad=models.ManyToManyField(Unidad,through='PefilUnidad')
    area=models.ManyToManyField(Area,through='PefilArea')

    def get_equipos_vigentes(self):
        relacion= self.relacionperfil_set.all().filter(esta_vigente=True).first()
        if relacion:
            return relacion.perfil
        else:
            return None
    

    def __str__(self):
        return '{}'.format(self.nombre)




 #....................................................................... Relacion Perfil= Secretaria ,Unidad ,Area  .............................................................

class PefilSecretaria(models.Model):
    secretaria = models.ForeignKey(Secretaria, null=True, blank=True, on_delete=models.CASCADE)
    perfil = models.ForeignKey(Perfil, null=True, blank=True, on_delete=models.CASCADE)
    esta_vigente=models.BooleanField(default=True)
    FechaInicio=models.DateField(auto_now_add=True,auto_now=False,blank=True)
    FechaFinal=models.DateField(blank=True,null=True)

class PefilArea(models.Model):
    area = models.ForeignKey(Area, null=True, on_delete=models.CASCADE, blank=True)
    perfil = models.ForeignKey(Perfil, null=True, blank=True, on_delete=models.CASCADE)
    esta_vigente=models.BooleanField(default=True)
    FechaInicio=models.DateField(auto_now_add=True,auto_now=False,blank=True)
    FechaFinal=models.DateField(blank=True, null=True)

class PefilUnidad(models.Model):
    unidad = models.ForeignKey(Unidad, null=True, on_delete=models.CASCADE, blank=True)
    perfil = models.ForeignKey(Perfil, null=True, blank=True, on_delete=models.CASCADE)
    esta_vigente=models.BooleanField(default=True)
    FechaInicio=models.DateField(auto_now_add=True,auto_now=False,blank=True)
    FechaFinal=models.DateField(blank=True, null=True)









 #...........................................................................Equipo ...........................................................

class Equipo(models.Model):
    nombre_host=models.CharField(max_length=50,unique=True)
    sistemas=(
        ('Windows XP','Windows XP'),
        ('Windows Vista','Windows Vista'),
        ('Windows 7','Windows 7'),
        ('Windows 8','Windows 8'),
        ('Windows 10','Windows 10'),
        ('Windows Server ','Windows Server '),
        ('Windows Server 2000 ','Windows Server 2000'),
        ('Windows Server 2003','Windows Server 2003'),
        ('Windows Server 2008','Windows Server 2008'),
        ('Windows Server 2012','Windows Server 2012'),
        ('Windows Server 2016','Windows Server 2016'),
        ('Windows Server 2019','Windows Server 2019'),
        ('Windows Small Business Server','Windows Small Business Server'),
        ('Windows Essential Business Server','Windows Essential Business Server'),
        ('Windows Home Server ','Windows Home Server '),
        ('Kali Linux','Kali Linux'),  
        ('Mac OS','Mac OS'),
        ('Ubuntu','Ubuntu'),     
        ('Ubuntu 5','Ubuntu 5'),
        ('Ubuntu 6','Ubuntu 6'),
        ('Ubuntu 7','Ubuntu 7'),
        ('Ubuntu 8','Ubuntu 8'),
        ('Ubuntu 9','Ubuntu 9'),
        ('Ubuntu 10','Ubuntu 10'),
        ('Ubuntu 11.04','Ubuntu 11.04'),
        ('Ubuntu 11.10','Ubuntu 11.10'),
        ('Ubuntu 12.04','Ubuntu 12.04'),
        ('Ubuntu 12.10','Ubuntu 12.10'),
        ('Ubuntu 13.04','Ubuntu 13.04'),
        ('Ubuntu 13.10','Ubuntu 13.10'),
        ('Ubuntu 14.04','Ubuntu 14.04'),
        ('Ubuntu 14.10','Ubuntu 14.10'),
        ('Ubuntu 15.04','Ubuntu 15.04'),
        ('Ubuntu 15.10','Ubuntu 15.10'),
        ('Ubuntu 16.04','Ubuntu 16.04'),
        ('Ubuntu 16.10','Ubuntu 16.10'),
        ('Ubuntu 17.04','Ubuntu 17.04'),
        ('Ubuntu 17.10','Ubuntu 17.10'),
        ('Ubuntu 18.04','Ubuntu 18.04'),
        ('Ubuntu 18.10','Ubuntu 18.10'),
        ('Ubuntu 19.04','Ubuntu 19.04'),
        ('Ubuntu 19.10','Ubuntu 19.10'),
        ('Ubuntu 20.04','Ubuntu 20.04'),
        ('Ubuntu 20.10','Ubuntu 20.10'),
        
    )
    sistema_operativo= models.CharField(max_length=100,choices=sistemas)
    mac=MACAddressField(blank=True,max_length=50, null=True)
    #MAC0=models.CharField(max_length=50)
    mac1=models.CharField(blank=True,max_length=50, null=True)
    
    #Adaptador=models.CharField(max_length=50)
    relacion1=models.ManyToManyField(Direccion,through='Relacion')
    perfil1=models.ManyToManyField(Perfil,through='RelacionPerfil')
    fecha= models.DateField(auto_now_add=True,auto_now=False,blank=True)
   
    #perfil1=models.ForeignKey(Perfil, null=True, blank=True, on_delete=models.CASCADE)
    
    def get_perfil_vigente(self):
        
        relacion= self.relacionperfil_set.all().filter(esta_vigente=True).first()
        if relacion:
            return relacion.perfil
        else:
            return None
    
    def __str__(self):
        return '{}'.format(self.nombre_host)

 #.................................................................Relacion Equipo = Direccion y Perfil  ........................................................

class Relacion(models.Model):
    direccion = models.ForeignKey(Direccion, null=True, blank=True, on_delete=models.CASCADE)
    equipo = models.ForeignKey(Equipo, null=True, blank=True, on_delete=models.CASCADE)
    esta_vigente=models.BooleanField(default=False)
    FechaInicio=models.DateField(auto_now_add=True,auto_now=False,blank=True)
    FechaFinal=models.DateField(blank=True,null=True)

class RelacionPerfil(models.Model):
    perfil = models.ForeignKey(Perfil, null=True, on_delete=models.CASCADE, blank=True)
    equipos = models.ForeignKey(Equipo, null=True, on_delete=models.CASCADE, blank=True)
    esta_vigente=models.BooleanField(default=False)
    FechaInicio=models.DateField(auto_now_add=True,auto_now=False,blank=True)
    FechaFinal=models.DateField(blank=True, null=True)
















































class Areauno(models.Model):
    nombre = models.CharField(null=True,db_index=True,max_length=33)
    direccion=models.CharField(max_length=33)
    detalle = models.CharField(max_length=50,unique=True,blank=True)
    def __str__(self):
        return '{}'.format(self.nombre)


class Unidaduno(models.Model):
    nombre = models.CharField(null=True,db_index=True,max_length=33)
    direccion=models.CharField(max_length=33)
    detalle = models.CharField(max_length=50,unique=True,blank=True)
    area=models.ForeignKey(Areauno, null=True, on_delete=models.CASCADE, blank=True)
    
    def __str__(self):
        return '{}'.format(self.nombre)


class Secretariauno(models.Model):
    nombre = models.CharField(null=True,db_index=True,max_length=33)
    direccion=models.CharField(max_length=33)
    detalle = models.CharField(max_length=50,unique=True,blank=True)
    unidad=models.ManyToManyField(Unidaduno,through='SecretariaUnidad')  
    area=models.ManyToManyField(Areauno,through='SecretariaArea')
    def __str__(self):
        return '{}'.format(self.nombre)


class SecretariaArea(models.Model):
    secretaria = models.ForeignKey(Secretariauno, null=True, blank=True, on_delete=models.CASCADE)
    area = models.ForeignKey(Areauno, null=True, blank=True, on_delete=models.CASCADE)
    FechaInicio=models.DateField(auto_now_add=True,auto_now=False,blank=True)

class SecretariaUnidad(models.Model):
    secretaria = models.ForeignKey(Secretariauno, null=True, blank=True, on_delete=models.CASCADE)
    unidad = models.ForeignKey(Unidaduno, null=True, blank=True, on_delete=models.CASCADE)
    FechaInicio=models.DateField(auto_now_add=True,auto_now=False,blank=True)







class Infraestructura(models.Model):
    
    #tipo de datos 
    nombre = models.CharField(max_length=50)
    direccion = models.CharField(max_length=50)
    detalle = models.CharField(max_length=50,unique=True,blank=True)
    vlan=models.CharField(max_length=50,default="Cliente",blank=True)
    fecha = models.DateField(auto_now_add=True,auto_now=False,blank=True)
    secretaria=models.ManyToManyField(Secretaria,through='InfraestructuraSecretaria')
    unidad=models.ManyToManyField(Unidad,through='InfraestructuraUnidad')  
    area=models.ManyToManyField(Area,through='InfraestructuraArea')
    
    def __str__(self):
        return '{}'.format(self.nombre)












class InfraestructuraSecretaria(models.Model):
    secretaria = models.ForeignKey(Secretaria, null=True, blank=True, on_delete=models.CASCADE)
    infraestructura = models.ForeignKey(Infraestructura, null=True, blank=True, on_delete=models.CASCADE)
    FechaInicio=models.DateField(auto_now_add=True,auto_now=False,blank=True)
   
class InfraestructuraUnidad(models.Model):
    unidad = models.ForeignKey(Unidad, null=True, blank=True, on_delete=models.CASCADE)
    infraestructura = models.ForeignKey(Infraestructura, null=True, blank=True, on_delete=models.CASCADE)
    FechaInicio=models.DateField(auto_now_add=True,auto_now=False,blank=True)

class InfraestructuraArea(models.Model):
    area = models.ForeignKey(Area, null=True, blank=True, on_delete=models.CASCADE)
    infraestructura = models.ForeignKey(Infraestructura, null=True, blank=True, on_delete=models.CASCADE)
    FechaInicio=models.DateField(auto_now_add=True,auto_now=False,blank=True)