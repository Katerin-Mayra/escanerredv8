from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.views import generic
from django.db.models import Q
from django.views.generic import ListView,CreateView,UpdateView,DeleteView,DetailView
from apps.principal.forms import DireccionFormulario ,EquipoFormulario,RelacionFormulario,PerfilFormulario,RelacionPerfilFormulario,RedFormulario
from apps.principal.forms import InfraestructuraFormulario,SecretariaFormulario,UnidadFormulario,AreaFormulario
from apps.principal.forms import InfraestructuraSecretariaFormulario,InfraestructuraUnidadFormulario,InfraestructuraAreaFormulario

from apps.principal.models import Direccion,Equipo,Relacion,Perfil,RelacionPerfil,Red,Infraestructura,Secretariauno,Unidaduno,Areauno

from apps.principal.models import InfraestructuraSecretaria,InfraestructuraUnidad,InfraestructuraArea

from apps.principal.models import SecretariaArea,SecretariaUnidad
from apps.principal.forms import SecretariaAreaFormulario,SecretariaUnidadFormulario

from apps.principal.models import PefilSecretaria,PefilArea,PefilUnidad

from apps.principal.models import Secretaria,Unidad,Area
from apps.principal.forms import SecretariavFormulario,AreavFormulario,UnidadvFormulario

from apps.principal.models import RedSecretaria,RedArea,RedUnidad
from apps.principal.forms import RedSecretariaFormulario,RedUnidadFormulario,RedAreaFormulario

from django.shortcuts import render_to_response
from django.shortcuts import render,get_object_or_404
# Create your views here.



## ..........................................:_::::::::::::.......Direccion..........::::::::::........................................

def index(request):
    return render(request,'principal/index.html')

def direccion_view(request):
    if request.method == 'POST':
        form = DireccionFormulario(request.POST)
        if form.is_valid():
            form.save()
            #return redirect('direccion:index')
       # return redirect('principal:index')
            return redirect(index)
    else:
        form = DireccionFormulario()
        
    return render(request,'principal/direccion/direccion_formulario.html',{'form_direccion':form})

def direccion_list(request):
    direccion= Direccion.objects.all().order_by('id')
    contexto={'direccionlist':direccion}
    return render(request,'principal/direccion/direccion_list.html',contexto)

#editar con funciones 
def direccion_edit(request,id_direccion):
    direccion=Direccion.objects.get(id=id_direccion)
    if request.method == 'GET':
        form =DireccionFormulario(instance=direccion)
    else:
        form=DireccionFormulario(request.POST,instance=direccion)
        if form.is_valid():
            form.save()
        return redirect(direccion_list)
    return render(request,'principal/direccion/direccion_formulario.html',{'form_direccion':form})

def direccion_delete(request,id_direccion):
    direccion=Direccion.objects.get(id=id_direccion)
    if request.method =='POST':
        direccion.delete()
        return redirect(direccion_list)
    return render(request,'principal/direccion/direccion_delete.html',{'direccion':direccion})

#................................................vistas basadas en clases DIRECCION ....................................................
#Lista de Direcciones
class DireccionList(ListView):
    model =Direccion
    template_name ='principal/direccion/direccion_list.html'
    paginate_by=20
    def get_queryset(self):
       return Direccion.objects.order_by('id')
    
    #def get_context_data(self,*args, **kwargs):
      
       # context=super().get_context_data(*args,**kwargs)
       # context['direccion_list']=self.get_object().relacion_set.all() 
       # return context



class DireccionCreate(CreateView):
    model =Direccion
    form_class=DireccionFormulario
    template_name='principal/direccion/direccion_formulario.html'
    success_url=reverse_lazy('direccion_listar')
    
class DirecionUpdate(UpdateView):
    model =Direccion
    form_class=DireccionFormulario
    template_name='principal/direccion/direccion_formulario.html'
    success_url=reverse_lazy('direccion_listar')
    
class DireccionDelete(DeleteView):
    model =Direccion
    form_class=DireccionFormulario
    template_name='principal/direccion/direccion_delete.html'
    success_url=reverse_lazy('direccion_listar')


class DireccionDetail(DetailView):
    model =Direccion
    template_name ='principal/direccion/direccion_list_detail.html'
    paginate_by=50
    def get_context_data(self,*args, **kwargs):
      
        context=super().get_context_data(*args,**kwargs)
        context['direccion_list']=self.get_object().relacion_set.all() #aca llamoa al modelo relacionperfil como objeto tambienpuedo hacerlo con otros modelos
        return context



#...........................................:::.....vistas basadas en clases Equipo ......::::..............................................


class EquipoList(ListView):
    model =Equipo
    template_name ='principal/equipo/equipo_list.html'
    paginate_by=20
    
    def get_queryset(self):
        return Equipo.objects.order_by('id')

class EquipoCreate(CreateView):
    model =Equipo
    form_class=EquipoFormulario
    template_name='principal/equipo/equipo_formulario.html'
    success_url=reverse_lazy('equipo_listar')
    
class EquipoUpdate(UpdateView):
    model =Equipo
    form_class=EquipoFormulario
    template_name='principal/equipo/equipo_formulario.html'
    success_url=reverse_lazy('equipo_listar')
    
class EquipoDelete(DeleteView):
    model =Equipo
    form_class=EquipoFormulario
    template_name='principal/equipo/equipo_delete.html'
    success_url=reverse_lazy('equipo_listar')

#...........................................:::.....vistas basadas en clases Relacion ......::::..............................................


class RelacionList(ListView):
    model =Relacion
    template_name ='principal/relacion/relacion_list.html'
    paginate_by=20
    def get_queryset(self):
        return Relacion.objects.order_by('id')

class RelacionCreate(CreateView):
    model =Relacion
    form_class=RelacionFormulario
    template_name='principal/relacion/relacion_formulario.html'
    success_url=reverse_lazy('relacion_listar')


    
class RelacionUpdate(UpdateView):
    model =Relacion
    form_class=RelacionFormulario
    template_name='principal/relacion/relacion_formulario.html'
    success_url=reverse_lazy('relacion_listar')
    
class RelacionDelete(DeleteView):
    model =Relacion
    form_class=RelacionFormulario
    template_name='principal/relacion/relacion_delete.html'
    success_url=reverse_lazy('relacion_listar')


class IP_RelacionCreate(CreateView):
    model =Relacion
    form_class=RelacionFormulario
    template_name='principal/relacion/ip_relacion_formulario.html'
    success_url=reverse_lazy('red_listar')
    def get_initial(self):

        direccion=Direccion.objects.get(id=self.kwargs.get("pk"))
        esta_vigente=True
        return {
            "direccion":direccion,
            "esta_vigente":esta_vigente

        }

class Equipo_ipRelacionCreate(CreateView):
    model =Relacion
    form_class=RelacionFormulario
    template_name='principal/relacion/equipo_ip_relacion_formulario.html'
    success_url=reverse_lazy('perfil_listar')
    def get_initial(self):
        equipo=Equipo.objects.get(id=self.kwargs.get("pk"))
        esta_vigente=True
        return {
            "equipo":equipo,
            "esta_vigente":esta_vigente
        }

class Equipo_ipRelacionCreateDos(CreateView):
    model =Relacion
    form_class=RelacionFormulario
    template_name='principal/relacion/equipo_ip_relacion_formulariodos.html'
    success_url=reverse_lazy('perfil_listar')
    def get_initial(self):
        equipo=Equipo.objects.get(id=self.kwargs.get("pk"))
        esta_vigente=True
        return {
            "equipo":equipo,
            "esta_vigente":esta_vigente
        }


   


#...........................................:::.....vistas basadas en clases Perfil ......::::..............................................


class PerfilList(ListView):
    model =Perfil
    template_name ='principal/perfil/perfil_list.html'
    paginate_by=20
    def get_queryset(self):
        return Perfil.objects.order_by('id')

class PerfilListall(ListView):
    model =Perfil
    template_name ='principal/perfil/perfil_list_all.html'
    paginate_by=20
    def get_queryset(self):
        return Perfil.objects.order_by('id')

class PerfilCreate(CreateView):
    model =Perfil
    form_class=PerfilFormulario
    template_name='principal/perfil/perfil_formulario.html'
    queryset=Perfil.objects.all()
    def get_success_url(self):
        return reverse_lazy('perfil_detail_list', kwargs={
            'pk':self.object.id
        })

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)
        

    
class PerfilUpdate(UpdateView):
    model =Perfil
    form_class=PerfilFormulario
    template_name='principal/perfil/perfil_formulario.html'
    success_url=reverse_lazy('perfil_listar_all')
    
class PerfilDelete(DeleteView):
    model =Perfil
    form_class=PerfilFormulario
    template_name='principal/perfil/perfil_delete.html'
    success_url=reverse_lazy('perfil_listar_all')


class PerfilDetail(DetailView):
    model =Perfil
    template_name='principal/perfil/perfil_detail_list.html'

    def get_context_data(self,*args, **kwargs):
      
        context=super().get_context_data(*args,**kwargs)
        context['equipo_list']=self.get_object().relacionperfil_set.all() 
        return context

    
    
   
#...........................................:::.....vistas basadas en clases Relacion Perfil ......::::..............................................


class RelacionPerfilList(ListView):
    model =RelacionPerfil
    template_name ='principal/relacionPerfil/relacionPerfil_list.html'
    paginate_by=20
    def get_queryset(self):
        return RelacionPerfil.objects.order_by('id')

class RelacionPerfilCreate(CreateView):
    model =RelacionPerfil
    form_class=RelacionPerfilFormulario
    template_name='principal/relacionPerfil/relacionPerfil_formulario.html'
    success_url=reverse_lazy('perfil_listar')
    
    def get_initial(self):

        perfil=Perfil.objects.get(id=self.kwargs.get("pk"))
        esta_vigente=True
        return {
            "perfil":perfil,
            "esta_vigente":esta_vigente
            
        }

class RelacionPerfilCreateDos(CreateView):
    model =RelacionPerfil
    form_class=RelacionPerfilFormulario
    template_name='principal/relacionPerfil/relacionPerfil_formulariodos.html'
    success_url=reverse_lazy('perfil_listar')
    
    def get_initial(self):

        perfil=Perfil.objects.get(id=self.kwargs.get("pk"))
        esta_vigente=True
        return {
            "perfil":perfil,
            "esta_vigente":esta_vigente
            
        }


    
class RelacionPerfilUpdate(UpdateView):
    model =RelacionPerfil
    form_class=RelacionPerfilFormulario
    template_name='principal/relacionPerfil/relacionPerfil_formulario.html'
    success_url=reverse_lazy('perfil_listar')
    
class RelacionPerfilDelete(DeleteView):
    model =RelacionPerfil
    form_class=RelacionPerfilFormulario
    template_name='principal/relacionPerfil/relacionPerfil_delete.html'
    success_url=reverse_lazy('perfil_listar')

#...........................................:::.....Vistas Genericas Perfil......::::..............................................


class PerfilEquipoDetail(DetailView):
    model =Perfil
    template_name ='principal/VistasUsuario/perfil_user_detail.html'
   
    def get_context_data(self,*args, **kwargs):
      
        context=super().get_context_data(*args,**kwargs)
        context['equipo_list']=self.get_object().relacionperfil_set.all() 
        return context


#############################################################################################################################
# Ver listas de equipos registrados por ususario INGENIERO
class PerfilDetailno(DetailView):
    model =Perfil
    template_name ='principal/VistasUsuario/perfil_detail.html'
    
    def get_context_data(self,*args, **kwargs):
      
        context=super().get_context_data(*args,**kwargs)
        context['equipo_list']=self.get_object().relacionperfil_set.all() #aca llamoa al modelo relacionperfil como objeto tambienpuedo hacerlo con otros modelos
        return context
###########################################################################################################################@@


#:.........................................................:::::.Lista de usuarios y Equipos :::................................   
  
    
#Mostrar Todos los usuarios y los equipos
class PerfilDetailAll(ListView):
    model =RelacionPerfil
  
    template_name ='principal/VistasUsuario/perfil_user_detail_all.html'
    paginate_by=20
    def get_queryset(self):
        return RelacionPerfil.objects.order_by('id')


#...........................................:::.....vistas basadas en clases Red ......::::..............................................




class REdList(ListView):
    model =Red
    template_name ='principal/red/red_list.html'
    paginate_by=20
    print(ListView.get)

    def get_queryset(self):
        buscar =self.request.GET.get('buscar')
        redes= Red.objects.all()

        if buscar:
            redes=Red.objects.filter(nombre__icontains=buscar )| Red.objects.filter(puerta_enlace__contains=buscar)
            try:
                redes=redes|Red.objects.filter(numero=int(buscar))
            except:
                pass
        return redes.order_by('id')



class REDCreate(CreateView):
    model =Red
    form_class=RedFormulario
    template_name='principal/red/red_formulario.html'
    
    def get_success_url(self):
        self.object.generarIps()
        return reverse_lazy('red_listar')


class REDCreateDos(CreateView):
    model =Red
    form_class=RedFormulario
    template_name='principal/red/red_formulario2.html'
    
    def get_success_url(self):
        self.object.generarIps()
        return reverse_lazy('red_listar')



class REDUpdate(UpdateView):
    model =Red
    form_class=RedFormulario
    template_name='principal/red/red_formulario.html'
    success_url=reverse_lazy('red_listar')
    
class REDDelete(DeleteView):
    model =Red
    form_class=RedFormulario
    template_name='principal/red/red_delete.html'
    success_url=reverse_lazy('red_listar')

class RedDetail(DetailView):
    model =Red
    template_name ='principal/red/red_detail.html'
    
    def get_context_data(self,*args, **kwargs):
        context=super().get_context_data(*args,**kwargs)
        context['red_list']=self.get_object().direccion_set.all()
        return context

    def get_queryset(self):
        buscar =self.request.GET.get('buscar')
        redes= Red.objects.all()

        if buscar:
            redes= Direccion.objects.filter(ip=buscar) 
        return redes.order_by('id')

    def get_initial(self):
        esta_vigente=True
        return {
            
            "esta_vigente":esta_vigente
            
        }

#...............................................................:::.....Infraestructura......::::...........................................................

class InfraestructuraList(ListView):
    model =Infraestructura
    template_name ='principal/infraestructura/infraestructura_list.html'
    paginate_by=20
    
    def get_queryset(self):
        return Infraestructura.objects.order_by('id')

class InfraestructuraCreate(CreateView):
    model =Infraestructura
    form_class=InfraestructuraFormulario
    template_name='principal/infraestructura/infraestructura_formulario.html'
    success_url=reverse_lazy('infraestructura_listar')
    queryset=Perfil.objects.all()
    def get_success_url(self):
        return reverse_lazy('infraestructura_detail_list', kwargs={
            'pk':self.object.id
        })

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)
        

    
class InfraestructuraUpdate(UpdateView):
    model =Infraestructura
    form_class=InfraestructuraFormulario
    template_name='principal/infraestructura/infraestructura_formulario.html'
    success_url=reverse_lazy('infraestructura_listar')
    
class InfraestructuraDelete(DeleteView):
    model =Infraestructura
    form_class=InfraestructuraFormulario
    template_name='principal/infraestructura/infraestructura_delete.html'
    success_url=reverse_lazy('infraestructura_listar')


class InfraestructuraDetail(DetailView):
    model =Infraestructura
    template_name='principal/infraestructura/infraestructura_detail_list.html'

    def get_context_data(self,*args, **kwargs):
      
        context=super().get_context_data(*args,**kwargs)
        context['infraestructura_list']=self.get_object().infraestructurasecretaria_set.all()
        
        context['infraestructuraunidad_list']=self.get_object().infraestructuraunidad_set.all() 
        return context


     


#...........................................:::.....Secretariauno......::::..............................................

class SecretariaList(ListView):
    model =Secretariauno
    template_name ='principal/secretaria/secretaria_list.html'
    paginate_by=20
    
    def get_queryset(self):
        return Secretariauno.objects.order_by('id')

class SecretariaCreate(CreateView):
    model =Secretariauno
    form_class=SecretariaFormulario
    template_name='principal/secretaria/secretaria_formulario.html'
    success_url=reverse_lazy('secretaria_listar')

    queryset=Secretariauno.objects.all()
    def get_success_url(self):
        return reverse_lazy('secretaria_detail_list', kwargs={
            'pk':self.object.id
        })

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)

class SecretariaCreateDos(CreateView):
    model =Secretariauno
    form_class=SecretariaFormulario
    template_name='principal/secretaria/secretaria_formulariodos.html'
    success_url=reverse_lazy('secretaria_listar')

    queryset=Secretariauno.objects.all()
    def get_success_url(self):
        return reverse_lazy('secretaria_detail_list', kwargs={
            'pk':self.object.id
        })

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)
    
class SecretariaUpdate(UpdateView):
    model =Secretariauno
    form_class=SecretariaFormulario
    template_name='principal/secretaria/secretaria_formulario.html'
    success_url=reverse_lazy('secretaria_listar')
    
class SecretariaDelete(DeleteView):
    model =Secretariauno
    form_class=SecretariaFormulario
    template_name='principal/secretaria/secretaria_delete.html'
    success_url=reverse_lazy('secretaria_listar')


class SecretariaDetail(DetailView):
    model =Secretariauno
    template_name='principal/secretaria/secretaria_detail_list.html'

    def get_context_data(self,*args, **kwargs):
      
        context=super().get_context_data(*args,**kwargs)
        context['secretaria_list']=self.get_object().secretariaarea_set.all() 
        return context


#...................................................................Secretaria Area.................................................
class SecretariaAreaList(ListView):
    model =SecretariaArea
    template_name ='principal/secretariaArea/secretariaArea_list.html'
    paginate_by=20
    
    def get_queryset(self):
        return SecretariaArea.objects.order_by('id')

class SecretariaAreaCreate(CreateView):
    model =SecretariaArea
    form_class=SecretariaAreaFormulario
    template_name='principal/secretariaArea/secretariaArea_formulario.html'
    success_url=reverse_lazy('secretaria_listar')

    queryset=Secretariauno.objects.all()
    def get_success_url(self):
        return reverse_lazy('secretaria_detail_list', kwargs={
            'pk':self.object.id
        })

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)

class SecretariaAreaCreateDos(CreateView):
    model =SecretariaArea
    form_class=SecretariaAreaFormulario
    template_name='principal/secretariaArea/secretariaArea_formulariodos.html'
    success_url=reverse_lazy('secretaria_listar')

    queryset=Secretariauno.objects.all()
    def get_success_url(self):
        return reverse_lazy('secretariaArea_detail_list', kwargs={
            'pk':self.object.id
        })

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)
    
class SecretariaAreaUpdate(UpdateView):
    model =SecretariaArea
    form_class=SecretariaFormulario
    template_name='principal/secretariaArea/secretariaArea_formulario.html'
    success_url=reverse_lazy('secretaria_listar')
    
class SecretariaAreaDelete(DeleteView):
    model =SecretariaArea
    form_class=SecretariaAreaFormulario
    template_name='principal/secretariaArea/secretariaArea_delete.html'
    success_url=reverse_lazy('secretaria_listar')


class SecretariaAreaDetail(DetailView):
    model =SecretariaArea
    template_name='principal/secretariaArea/secretariaArea_detail_list.html'

    def get_context_data(self,*args, **kwargs):
      
        context=super().get_context_data(*args,**kwargs)
        context['secretaria_list']=self.get_object().area_set.all() 
        return context


#...................................................................Secretaria unidad.................................................
class SecretariaUnidadList(ListView):
    model =Secretariauno
    template_name ='principal/secretaria/secretaria_list.html'
    paginate_by=20
    
    def get_queryset(self):
        return Secretariauno.objects.order_by('id')

class SecretariaUnidadCreate(CreateView):
    model =Secretariauno
    form_class=SecretariaFormulario
    template_name='principal/secretaria/secretaria_formulario.html'
    success_url=reverse_lazy('secretaria_listar')

    queryset=Secretariauno.objects.all()
    def get_success_url(self):
        return reverse_lazy('secretaria_detail_list', kwargs={
            'pk':self.object.id
        })

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)

class SecretariaUnidadCreateDos(CreateView):
    model =Secretariauno
    form_class=SecretariaFormulario
    template_name='principal/secretaria/secretaria_formulariodos.html'
    success_url=reverse_lazy('secretaria_listar')

    queryset=Secretariauno.objects.all()
    def get_success_url(self):
        return reverse_lazy('secretaria_detail_list', kwargs={
            'pk':self.object.id
        })

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)
    
class SecretariaUnidadUpdate(UpdateView):
    model =Secretariauno
    form_class=SecretariaFormulario
    template_name='principal/secretaria/secretaria_formulario.html'
    success_url=reverse_lazy('secretaria_listar')
    
class SecretariaUnidadDelete(DeleteView):
    model =Secretariauno
    form_class=SecretariaFormulario
    template_name='principal/secretaria/secretaria_delete.html'
    success_url=reverse_lazy('secretaria_listar')


class SecretariaUnidadDetail(DetailView):
    model =Secretariauno
    template_name='principal/secretaria/secretaria_detail_list.html'

    def get_context_data(self,*args, **kwargs):
      
        context=super().get_context_data(*args,**kwargs)
        context['secretaria_list']=self.get_object().area_set.all() 
        return context






#...........................................:::.....Unidad......::::..............................................

class UnidadList(ListView):
    model =Unidaduno
    template_name ='principal/unidad/unidad_list.html'
    paginate_by=20
    
    def get_queryset(self):
        return Unidaduno.objects.order_by('id')

class UnidadCreate(CreateView):
    model =Unidaduno
    form_class=UnidadFormulario
    template_name='principal/unidad/unidad_formulario.html'
    success_url=reverse_lazy('unidad_listar')


class UnidadCreateDos(CreateView):
    model =Unidaduno
    form_class=UnidadFormulario
    template_name='principal/unidad/unidad_formulario.html'
    success_url=reverse_lazy('unidad_listar')
    
class UnidadUpdate(UpdateView):
    model =Unidaduno
    form_class=UnidadFormulario
    template_name='principal/unidad/unidad_formulario.html'
    success_url=reverse_lazy('unidad_listar')
    
class UnidadDelete(DeleteView):
    model =Unidaduno
    form_class=UnidadFormulario
    template_name='principal/unidad/unidad_delete.html'
    success_url=reverse_lazy('unidad_listar')


class UnidadDetail(DetailView):
    model =Unidaduno
    template_name='principal/unidad/unidad_detail_list.html'

    def get_context_data(self,*args, **kwargs):
      
        context=super().get_context_data(*args,**kwargs)
        context['equipo_list']=self.get_object().relacionperfil_set.all() 
        return context


#...........................................:::.....Area......::::..............................................

class AreaList(ListView):
    model =Areauno
    template_name ='principal/area/area_list.html'
    paginate_by=20
    
    def get_queryset(self):
        return Areauno.objects.order_by('id')

class AreaCreate(CreateView):
    model =Areauno
    form_class=AreaFormulario
    template_name='principal/area/area_formulario.html'
    success_url=reverse_lazy('area_listar')
    
class AreaUpdate(UpdateView):
    model =Areauno
    form_class=AreaFormulario
    template_name='principal/area/area_formulario.html'
    success_url=reverse_lazy('area_listar')
    
class AreaDelete(DeleteView):
    model =Areauno
    form_class=AreaFormulario
    template_name='principal/area/area_delete.html'
    success_url=reverse_lazy('area_listar')


class AreaDetail(DetailView):
    model =Areauno
    template_name='principal/area/area_detail_list.html'

    def get_context_data(self,*args, **kwargs):
      
        context=super().get_context_data(*args,**kwargs)
        context['equipo_list']=self.get_object().relacionperfil_set.all() 
        return context




#:_________________________________________________________________________________   SECRETARIA  ___________________________________________________________

#...........................................:::.....Secretaria......::::..............................................

class SecretariavList(ListView):
    model =Secretaria
    template_name ='principal/secretariav/secretariav_list.html'
    paginate_by=20
    
    def get_queryset(self):
        return Secretaria.objects.order_by('id')

class SecretariavCreate(CreateView):
    model =Secretaria
    form_class=SecretariavFormulario
    template_name='principal/secretariav/secretariav_formulario.html'
    success_url=reverse_lazy('secretariav_listar')

    queryset=Secretaria.objects.all()
    def get_success_url(self):
        return reverse_lazy('secretariav_detail_list', kwargs={
            'pk':self.object.id
        })

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)

class SecretariavCreateDos(CreateView):
    model =Secretaria
    form_class=SecretariavFormulario
    template_name='principal/secretariav/secretariav_formulariodos.html'
    success_url=reverse_lazy('secretariav_listar')

    queryset=Secretaria.objects.all()
    def get_success_url(self):
        return reverse_lazy('secretariav_detail_list', kwargs={
            'pk':self.object.id
        })

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)
    
class SecretariavUpdate(UpdateView):
    model =Secretaria
    form_class=SecretariavFormulario
    template_name='principal/secretariav/secretariav_formulariodos.html'
    success_url=reverse_lazy('secretariav_listar')
    
class SecretariavDelete(DeleteView):
    model =Secretaria
    form_class=SecretariavFormulario
    template_name='principal/secretariav/secretariav_delete.html'
    success_url=reverse_lazy('secretariav_listar')


class SecretariavDetail(DetailView):
    model =Secretaria
    template_name='principal/secretariav/secretariav_detail_list.html'

    def get_context_data(self, **kwargs):
        context = super(SecretariavDetail, self).get_context_data(**kwargs)
        context['unidades_list'] = Unidad.objects.filter(secretaria=self.object)
        context['areas_list'] = Area.objects.filter(secretaria=self.object)
        return context

   



#:_________________________________________________________________________________   UNIDAD  ___________________________________________________________

#...........................................:::.....unidad verdadero......::::..............................................

class UnidadvList(ListView):
    model =Unidad
    template_name ='principal/unidadv/unidadv_list.html'
    paginate_by=20
    
    def get_queryset(self):
        return Unidad.objects.order_by('id')

class UnidadvCreate(CreateView):
    model =Unidad
    form_class=UnidadvFormulario
    template_name='principal/unidadv/unidadv_formulario.html'
    success_url=reverse_lazy('secretariav_listar')

    queryset=Unidad.objects.all()

    def get_initial(self):

        secretaria=Secretaria.objects.get(id=self.kwargs.get("pk"))
    
        return {
            "secretaria":secretaria,
        }


    def get_context_data(self,**kwargs):
        context=super().get_context_data(**kwargs)
        return context

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)

class UnidadvCreateDos(CreateView):
    model =Unidad
    form_class=UnidadvFormulario
    template_name='principal/unidadv/unidadv_formulariodos.html'
    success_url=reverse_lazy('secretariav_listar')

    queryset=Unidad.objects.all()
    


    def get_initial(self):

        secretaria=Secretaria.objects.get(id=self.kwargs.get("pk"))
    
        return {
            "secretaria":secretaria,
         
            
        }

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)
    
class UnidadvUpdate(UpdateView):
    model =Unidad
    form_class=UnidadvFormulario
    template_name='principal/unidadv/unidadv_formulariodos.html'
    success_url=reverse_lazy('secretariav_listar')
    
class UnidadvDelete(DeleteView):
    model =Unidad
    form_class=UnidadvFormulario
    template_name='principal/unidadv/unidadv_delete.html'
    success_url=reverse_lazy('secretariav_listar')

class UnidadvDeletedos(DeleteView):
    model =Unidad
    form_class=UnidadvFormulario
    template_name='principal/unidadv/unidadv_deletedos.html'
    success_url=reverse_lazy('secretariav_listar')


class UnidadvDetail(DetailView):
    model =Unidad
    template_name='principal/unidadv/unidadv_detail_list.html'

    def get_context_data(self, **kwargs):
        context = super(UnidadvDetail, self).get_context_data(**kwargs)
        context['areas_list'] = Area.objects.filter(unidad=self.object)
        return context

class UnidadvDetailView(DetailView):
    model =Unidad
    template_name='principal/unidadv/unidadv_detail_list_view.html'

    def get_context_data(self, **kwargs):
        context = super(UnidadvDetailView, self).get_context_data(**kwargs)
        context['areas_list'] = Area.objects.filter(unidad=self.object)
        return context


#:_________________________________________________________________________________   AREA  ___________________________________________________________

#...........................................:::.....area verdadero......::::..............................................

class AreavList(ListView):
    model =Area
    template_name ='principal/areav/areav_list.html'
    paginate_by=20
    
    def get_queryset(self):
        return Area.objects.order_by('id')

class AreavCreate(CreateView):
    model =Area
    form_class=AreavFormulario
    template_name='principal/areav/areav_formulario.html'
    success_url=reverse_lazy('secretariav_listar')

    queryset=Area.objects.all()
    

    def get_initial(self):

        secretaria=Secretaria.objects.get(id=self.kwargs.get("pk"))
    
        return {
            "secretaria":secretaria,
         
            
        }


    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)

class AreavCreateDos(CreateView):
    model =Area
    form_class=AreavFormulario
    template_name='principal/areav/areav_formulariodos.html'
    success_url=reverse_lazy('secretariav_listar')

    queryset=Area.objects.all()
 


    def get_initial(self):

        secretaria=Secretaria.objects.get(id=self.kwargs.get("pk"))
    
        return {
            "secretaria":secretaria,
         
            
        }

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)




class AreavUniCreate(CreateView):
    model =Area
    form_class=AreavFormulario
    template_name='principal/areav/areav_formularioUni.html'
    success_url=reverse_lazy('areav_listar')

    queryset=Area.objects.all()
    def get_success_url(self):
        return reverse_lazy('areav_detail_list', kwargs={
            'pk':self.object.id
        })

    def get_initial(self):

        unidad=Unidad.objects.get(id=self.kwargs.get("pk"))
    
        return {
            "unidad":unidad,
         
            
        }


    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)

class AreavUniCreateDos(CreateView):
    model =Area
    form_class=AreavFormulario
    template_name='principal/areav/areav_formularioUnidos.html'
    success_url=reverse_lazy('areav_listar')

    queryset=Area.objects.all()
    def get_success_url(self):
        return reverse_lazy('areav_detail_list', kwargs={
            'pk':self.object.id
        })


    def get_initial(self):

        unidad=Unidad.objects.get(id=self.kwargs.get("pk"))
    
        return {
            "unidad":unidad,
         
            
        }

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)



    
class AreavUpdate(UpdateView):
    model =Area
    form_class=AreavFormulario
    template_name='principal/areav/areav_formularioUnidos.html'
    success_url=reverse_lazy('unidadv_listar')
    
class AreavDelete(DeleteView):
    model =Area
    form_class=AreavFormulario
    template_name='principal/areav/areav_delete.html'
    success_url=reverse_lazy('secretaria_listar')


class AreavDetail(DetailView):
    model =Area
    template_name='principal/areav/areav_detail_list.html'

    def get_context_data(self, **kwargs):
        context = super(AreavDetail, self).get_context_data(**kwargs)
        context['areas_list'] = Area.objects.filter(unidad=self.object)
        return context








#:_________________________________________________________________________________   RELACION RED SECRETARIA  ___________________________________________________________

#...........................................:::.....RED - Secretaria......::::..............................................

class RedSecretariaList(ListView):
    model =RedSecretaria
    template_name ='principal/redSecretaria/redSecretaria_list.html'
    paginate_by=20
    
    def get_queryset(self):
        return RedSecretaria.objects.order_by('id')

class RedSecretariaCreate(CreateView):
    model =RedSecretaria
    form_class=RedSecretariaFormulario
    template_name='principal/redSecretaria/redSecretaria_formulario.html'
    success_url=reverse_lazy('secretariav_listar')

    queryset=RedSecretaria.objects.all()
    def get_success_url(self):
        return reverse_lazy('secretariav_detail_list', kwargs={
            'pk':self.object.id
        })

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)

class RedSecretariaCreateDos(CreateView):
    model =RedSecretaria
    form_class=RedSecretariaFormulario
    template_name='principal/redSecretaria/redSecretaria_formulariodos.html'
    success_url=reverse_lazy('secretariav_listar')

    queryset=RedSecretaria.objects.all()
    def get_success_url(self):
        return reverse_lazy('secretariav_detail_list', kwargs={
            'pk':self.object.id
        })

    def form_valid(self, form):
        print(form.cleaned_data)
        return super().form_valid(form)
    
class RedSecretariaUpdate(UpdateView):
    model =RedSecretaria
    form_class=RedSecretariaFormulario
    template_name='principal/redSecretaria/redSecretaria_formulariodos.html'
    success_url=reverse_lazy('secretariav_listar')
    
class RedSecretariaDelete(DeleteView):
    model =RedSecretaria
    form_class=SecretariavFormulario
    template_name='principal/redSecretaria/redSecretaria_delete.html'
    success_url=reverse_lazy('secretariav_listar')


class RedSecretariaDetail(DetailView):
    model =RedSecretaria
    template_name='principal/redSecretaria/redSecretaria_detail_list.html'

    def get_context_data(self, **kwargs):
        context = super(SecretariavDetail, self).get_context_data(**kwargs)
        context['unidades_list'] = Unidad.objects.filter(secretaria=self.object)
        context['areas_list'] = Area.objects.filter(secretaria=self.object)
        return context


























































































































































































#...........................................:::.....Relacion  InfraestructuraSecretaria......::::..............................................

class InfraestructuraSecretariaList(ListView):
    model =InfraestructuraSecretaria
    template_name ='principal/infraestructuraSecretaria/infraestructuraSecretaria_list.html'
    paginate_by=20
    
    def get_queryset(self):
        return InfraestructuraSecretaria.objects.order_by('id')

class InfraestructuraSecretariaCreate(CreateView):
    model =InfraestructuraSecretaria
    form_class=InfraestructuraSecretariaFormulario
    template_name='principal/infraestructuraSecretaria/infraestructuraSecretaria_formulario.html'
    success_url=reverse_lazy('infraestructuraSecretaria_listar')
    def get_initial(self):

        infraestructura=Infraestructura.objects.get(id=self.kwargs.get("pk"))
    
        return {
            "infraestructura":infraestructura,
         
            
        }

class InfraestructuraSecretariaCreatedos(CreateView):
    model =InfraestructuraSecretaria
    form_class=InfraestructuraSecretariaFormulario
    template_name='principal/infraestructuraSecretaria/infraestructuraSecretaria_formulariodos.html'
    success_url=reverse_lazy('infraestructuraSecretaria_listar')
    def get_initial(self):

        infraestructura=Infraestructura.objects.get(id=self.kwargs.get("pk"))
    
        return {
            "infraestructura":infraestructura,
        
        }
    

class InfraestructuraSecretariaUpdate(UpdateView):
    model =InfraestructuraSecretaria
    form_class=AreaFormulario
    template_name='principal/infraestructuraSecretaria/infraestructuraSecretaria_formulario.html'
    success_url=reverse_lazy('area_listar')
    
class InfraestructuraSecretariaDelete(DeleteView):
    model =InfraestructuraSecretaria
    form_class=AreaFormulario
    template_name='principal/infraestructuraSecretaria/infraestructuraSecretaria_delete.html'
    success_url=reverse_lazy('area_listar')


class InfraestructuraSecretariaDetail(DetailView):
    model =InfraestructuraSecretaria
    template_name='principal/InfraestructuraSecretaria/infraestructuraSecretaria_detail_list.html'

    def get_context_data(self,*args, **kwargs):
      
        context=super().get_context_data(*args,**kwargs)
        context['equipo_list']=self.get_object().relacionperfil_set.all() 
        return context

#...........................................:::.....Relacion  InfraestructuraUnidad  ......::::..............................................

class InfraestructuraUnidadList(ListView):
    model =InfraestructuraUnidad
    template_name ='principal/infraestructuraUnidad/infraestructuraSecretaria_list.html'
    paginate_by=20
    
    def get_queryset(self):
        return InfraestructuraUnidad.objects.order_by('id')

class InfraestructuraUnidadCreate(CreateView):
    model =InfraestructuraUnidad
    form_class=InfraestructuraUnidadFormulario
    template_name='principal/infraestructuraUnidad/infraestructuraUnidad_formulario.html'
    
    success_url=reverse_lazy('infraestructuraUnidad_listar')
    
    def get_initial(self):

        infraestructura=Infraestructura.objects.get(id=self.kwargs.get("pk"))
    
        return {
            "infraestructura":infraestructura,
         
            
        }

class InfraestructuraUnidadCreateDos(CreateView):
    model =InfraestructuraUnidad
    form_class=InfraestructuraUnidadFormulario
    template_name='principal/infraestructuraUnidad/infraestructuraUnidad_formulariodos.html'
    

    success_url=reverse_lazy('infraestructuraUnidad_listar')
    def get_initial(self):

        infraestructura=Infraestructura.objects.get(id=self.kwargs.get("pk"))
    
        return {
            "infraestructura":infraestructura,
         
            
        }


    
class InfraestructuraUnidadUpdate(UpdateView):
    model =InfraestructuraUnidad
    form_class=AreaFormulario
    template_name='principal/infraestructuraUnidad/infraestructuraUnidad_formulario.html'
    success_url=reverse_lazy('area_listar')
    
class InfraestructuraUnidadDelete(DeleteView):
    model =InfraestructuraUnidad
    form_class=AreaFormulario
    template_name='principal/infraestructuraUnidad/infraestructuraUnidad_delete.html'
    success_url=reverse_lazy('area_listar')


class InfraestructuraUnidadDetail(DetailView):
    model =InfraestructuraUnidad
    template_name='principal/infraestructuraUnidad/infraestructuraUnidad_detail_list.html'

    def get_context_data(self,*args, **kwargs):
      
        context=super().get_context_data(*args,**kwargs)
        context['equipo_list']=self.get_object().relacionperfil_set.all() 
        return context


#...........................................:::.....Relacion  InfraestructuraArea ......::::..............................................

class InfraestructuraAreaList(ListView):
    model =InfraestructuraArea
    template_name ='principal/infraestructuraArea/infraestructuraArea_list.html'
    paginate_by=20
    
    def get_queryset(self):
        return InfraestructuraArea.objects.order_by('id')

class InfraestructuraAreaCreate(CreateView):
    model =InfraestructuraArea
    form_class=AreaFormulario
    template_name='principal/infraestructuraArea/infraestructuraArea_formulario.html'
    success_url=reverse_lazy('area_listar')
    
class InfraestructuraAreaUpdate(UpdateView):
    model =InfraestructuraArea
    form_class=AreaFormulario
    template_name='principal/infraestructuraArea/infraestructuraArea_formulario.html'
    success_url=reverse_lazy('area_listar')
    
class InfraestructuraAreaDelete(DeleteView):
    model =InfraestructuraArea
    form_class=AreaFormulario
    template_name='principal/infraestructuraArea/infraestructuraArea_delete.html'
    success_url=reverse_lazy('area_listar')


class InfraestructuraUnidadDetail(DetailView):
    model =InfraestructuraArea
    template_name='principal/infraestructuraArea/infraestructuraArea_detail_list.html'

    def get_context_data(self,*args, **kwargs):
      
        context=super().get_context_data(*args,**kwargs)
        context['equipo_list']=self.get_object().relacionperfil_set.all() 
        return context