from django import forms
from datetime import date
from apps.principal.models import Direccion,Equipo,Relacion,Perfil,RelacionPerfil,Red,Infraestructura,Secretariauno,Unidaduno,Areauno
from apps.principal.models import Secretaria,Unidad,Area
from apps.principal.models import InfraestructuraSecretaria,InfraestructuraUnidad,InfraestructuraArea
from apps.principal.models import SecretariaArea,SecretariaUnidad
from apps.principal.models import PefilSecretaria,PefilArea,PefilUnidad
from apps.principal.models import RedSecretaria,RedArea,RedUnidad

class DireccionFormulario(forms.ModelForm):

    class Meta:
        model= Direccion
        fields=[
            'ip',
            'puerta_enlace',
            'mascara',
        ]
        labels={
            'ip':'IP',
            'puerta_enlace':'Puerta Enlace',
            'mascara':'Mascara',
        }
        widgets={
            'ip':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'Ingrese una IP',
                'id':'IP'
                
                }),
            'puerta_enlace':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'Ingrese la puerta de enlace',
                'id':'puerta de enlace'
                
                }),
            'mascara':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'Ingrese una mascara',
                'id':'mascara'
                
                }),
        }

class EquipoFormulario(forms.ModelForm):

    class Meta:
        model= Equipo
        fields=[
            'nombre_host',
            'sistema_operativo',
            'mac',
            'mac1',
           
        ]
        labels={
            'nombre_host':'Nombre',
            'sistema_operativo':'Sistema Operativo',
            'mac':'MAC',
            'mac1':'MAC1',
   
        }
        widgets={
            'nombre_host':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'Ingrese nombre del equipo',
                'id':'nombre_host'
                }),
            'sistema_operativo':forms.Select(attrs={
                'class':'form-control',
                'placeholder':'Ingrese el sistema_operativo',
                'id':'sistema_operativo'
                
                }),
            'mac':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'Ingrese mac',
                'id':'mac'
                
                }),
            'mac1':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'Ingrese mac',
                'id':'mac1',
                
                }),
            #'relacion1':forms.Select(attrs={'class' :'form-control'}),
        }

class DateInput(forms.DateInput):
    input_type='date'

class RelacionFormulario(forms.ModelForm):

    class Meta:
        model= Relacion
        fields=[
            'direccion',
            'equipo',
            'esta_vigente',
            #'FechaInicio',
            'FechaFinal',
           
        ]
        labels={

            'direccion':'Direccion',
            'equipo':'Equipo',
            'esta_vigente':'esta_vigente',
            #'FechaInicio':'Fecha Inicio',
            'FechaFinal':'Fecha Final',
   
        }
        widgets={

            'direccion':forms.Select(attrs={
                
                    'class' :'form-control select2',
                    'placeholder':'Ingrese equipo',
                    
                }),
            'equipo':forms.Select(attrs={
                    'class' :'form-control ',
                    
                }),
             'esta_vigente':forms.TextInput(attrs={
                'class' :'form-control'
                }),
            #'FechaInicio':forms.DateInput(),
            #'FechaInicio':forms.DateField(widget=DateInput),
            #'FechaInicio':forms.TextInput(attrs={'class' :'form-control'}),
            'FechaFinal':forms.TextInput(attrs={'class' :'form-control'}),
            #'FechaFinal':forms.DateInput(format='%Y/%m/%d'),

        
        }

class PerfilFormulario(forms.ModelForm):

    class Meta:
        model= Perfil
        fields=[
            'nombre',
            'ci',
            'apellido',
            'correo',
            'tipo',
            #'fecha',
           
           
        ]
        labels={

            'nombre':'Nombre',
            'ci':'CI',
            'apellido':'Apellido',
            'correo':'Correo Electronico',
            'tipo':'Tipo',
            #'fecha':'Fecha',
         
   
        }
        widgets={

            'nombre':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'Ingrese  Nombre',
                'id':'nombre'
                }),
            'ci':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'Ingrese una CI',
                'id':'ci'
                }),
            'apellido':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'Ingrese Apellido',
                'id':'apellido'
                }),
            'correo':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'Ingrese Correo Electronico',
                'id':'correo'
                }),
         
            #'fecha':forms.TextInput(attrs={'class' :'form-control'}),
           # 'equipo':forms.SelectMultiple(attrs={'class':'from-control'}),

        
        }

class RelacionPerfilFormulario(forms.ModelForm):

    class Meta:
        model= RelacionPerfil
        fields=[
            'perfil',
            'equipos',
            'esta_vigente',
            #'FechaInicio',
            'FechaFinal',
           
        ]
        labels={

            'perfil':'Usuario',
            'equipos':'Equipos',
            'esta_vigente':'esta_vigente',
            #'FechaInicio':'Fecha Inicio',
            'FechaFinal':'Fecha Final',
   
        }
        widgets={

            'perfil':forms.Select(attrs={
                'class' :'form-control',
              

            }),
            'equipos':forms.Select(attrs={
                'class' :'form-control',
                'placeholder':'Agrege el Equipo',
                'id':'equipos'
                }),
            'esta_vigente':forms.TextInput(attrs={
                'class' :'form-control'
                }),
            #'FechaInicio':forms.DateInput(),
            #'FechaInicio':forms.DateField(widget=DateInput),
            #'FechaInicio':forms.TextInput(attrs={'class' :'form-control'}),
            'FechaFinal':forms.TextInput(attrs={'class' :'form-control'}),
            #'FechaFinal':forms.DateInput(format='%Y/%m/%d'),

           

        
        }

class RedFormulario(forms.ModelForm):

    class Meta:
        model= Red
        fields=[
            'numero',
            'puerta_enlace',
            'nombre',
            'mascara',
            'inicio',
            'fin',

           
        ]
        labels={

            'numero':'Numero de Red',
            'puerta_enlace':'Puerta de enlace',
            'nombre':'Nombre',
            'mascara':'Mascara',
            'inicio':'Inicio',
            'fin':'Fin',

   
        }
        widgets={

        
            'numero':forms.TextInput(attrs={
                'class' :'form-control',
                'placeholder':'Agrege numero de Red',
                'id':'numero'
                }),
            'puerta_enlace':forms.TextInput(attrs={
                'class' :'form-control',
                'placeholder':'Agrege puerta de enlace',
                'id':'puerta_enlace'
                }),
            'nombre':forms.TextInput(attrs={
                'class' :'form-control',
                'placeholder':'Agrege Nombre de Red',
                'id':'nombre'
                }),
            'mascara':forms.TextInput(attrs={
                'class' :'form-control',
                'placeholder':'Agrege Mascara',
                'id':'mascara'
                }),
            'inicio':forms.TextInput(attrs={
                'class' :'form-control',
                'placeholder':'Agrege numero de inicio',
                'id':'inicio'
                }),
            'fin':forms.TextInput(attrs={
                'class' :'form-control',
                'placeholder':'Agrege numero de fin',
                'id':'fin'
                }),


        
        }

class InfraestructuraFormulario(forms.ModelForm):

    class Meta:
        model= Infraestructura
        fields=[
            'nombre',
            'direccion',
            'detalle',
  
        
           
        ]
        labels={

            'nombre':'nombre',
            'direccion':'direccion',
            'detalle':'detalle',
            

   
        }
        widgets={


            'nombre':forms.TextInput(attrs={
                'class' :'form-control',
                'placeholder':'Agrege numero de Red',
                'id':'nombre'
                }),
                
            'direccion':forms.TextInput(attrs={
                'class' :'form-control',
                'placeholder':'Agrege numero de Red',
                'id':'direccion'
                }),
            'detalle':forms.TextInput(attrs={
                'class' :'form-control',
                'placeholder':'Agrege numero de Red',
                'id':'detalle'
                }),
        
        }

class SecretariaFormulario(forms.ModelForm):

    class Meta:
        model= Secretariauno
        fields=[
            'nombre',
            'direccion',
            'detalle',
  
        
           
        ]
        labels={

            'nombre':'nombre',
            'direccion':'direccion',
            'detalle':'detalle',
            

   
        }
        widgets={


            'nombre':forms.TextInput(attrs={
                'class' :'form-control',
                'placeholder':'Agrege numero de Red',
                'id':'nombre'
                }),
                
            'direccion':forms.TextInput(attrs={
                'class' :'form-control',
                'placeholder':'Agrege numero de Red',
                'id':'direccion'
                }),
            'detalle':forms.TextInput(attrs={
                'class' :'form-control',
                'placeholder':'Agrege numero de Red',
                'id':'detalle'
                }),
 
   

        
        }


class UnidadFormulario(forms.ModelForm):

    class Meta:
        model= Unidaduno
        fields=[
            'nombre',
            'direccion',
            'detalle',
  
        
           
        ]
        labels={

            'nombre':'nombre',
            'direccion':'direccion',
            'detalle':'detalle',
            

   
        }
        widgets={


            'nombre':forms.TextInput(attrs={
                'class' :'form-control',
                'placeholder':'Agrege numero de Red',
                'id':'nombre'
                }),
                
            'direccion':forms.TextInput(attrs={
                'class' :'form-control',
                'placeholder':'Agrege numero de Red',
                'id':'direccion'
                }),
            'detalle':forms.TextInput(attrs={
                'class' :'form-control',
                'placeholder':'Agrege numero de Red',
                'id':'detalle'
                }),
 
        }

class AreaFormulario(forms.ModelForm):

    class Meta:
        model= Areauno
        fields=[
            'nombre',
            'direccion',
            'detalle',
  
        
           
        ]
        labels={

            'nombre':'nombre',
            'direccion':'direccion',
            'detalle':'detalle',
            

   
        }
        widgets={


            'nombre':forms.TextInput(attrs={
                'class' :'form-control',
                'placeholder':'Agrege numero de Red',
                'id':'nombre'
                }),
                
            'direccion':forms.TextInput(attrs={
                'class' :'form-control',
                'placeholder':'Agrege numero de Red',
                'id':'direccion'
                }),
            'detalle':forms.TextInput(attrs={
                'class' :'form-control',
                'placeholder':'Agrege numero de Red',
                'id':'detalle'
                }),
 
   

        
        }



#__________________________________________________-SECRETARIA VERSADERO ________________________________
class SecretariavFormulario(forms.ModelForm):

    class Meta:
        model= Secretaria
        fields=[
            'nombre',
            'sigla',
            'tipo',
            'detalle',
            'etiqueta',
  
        
           
        ]
        labels={

            'nombre':'Nombre',
            'sigla':'Sigla',
            'tipo':'Tipo',
            'detalle':'Direccion',
       
            'etiqueta':'Etiqueta',
            

   
        }
        widgets={



            'nombre':forms.TextInput(attrs={
                'class' :'form-control',
                'placeholder':'Agrege numero de Red',
                'id':'nombre'
                }),
            'sigla':forms.TextInput(attrs={
                'class' :'form-control',
                'placeholder':'Agrege numero de sigla',
                'id':'sigla'
                }),
            'tipo':forms.TextInput(attrs={
                'class' :'form-control',
                'placeholder':'Agrege numero de tipo',
                'id':'tipo'
                }),

            'detalle':forms.TextInput(attrs={
                'class' :'form-control',
                'placeholder':'Agrege Direccion',
                'id':'detalle'
                }),

      

            'etiqueta':forms.TextInput(attrs={
                'class' :'form-control',
                'placeholder':'Agrege numero de etiqueta',
                'id':'etiqueta'
                }),
 
   

        
        }








#__________________________________________________-AREA VERSADERO ________________________________
class AreavFormulario(forms.ModelForm):

    class Meta:
        model= Area
        fields=[
            'nombre',
            'sigla',
            'tipo',
            'detalle',
            'etiqueta',
            'secretaria',
            'unidad',
  
        
           
        ]
        labels={

            'nombre':'Nombre',
            'sigla':'Sigla',
            'tipo':'Tipo',
            'detalle':'Direccion',
            'etiqueta':'Etiqueta',
            'secretaria':'Secretaria',
            'unidad':'Unidad',
            

   
        }
        widgets={



            'nombre':forms.TextInput(attrs={
                'class' :'form-control',
                'placeholder':'Agrege numero de Red',
                'id':'nombre'
                }),
            'sigla':forms.TextInput(attrs={
                'class' :'form-control',
                'placeholder':'Agrege numero de sigla',
                'id':'sigla'
                }),
            'tipo':forms.TextInput(attrs={
                'class' :'form-control',
                'placeholder':'Agrege numero de tipo',
                'id':'tipo'
                }),

            'detalle':forms.TextInput(attrs={
                'class' :'form-control',
                'placeholder':'Agrege Direccion',
                'id':'detalle'
                }),

            'etiqueta':forms.TextInput(attrs={
                'class' :'form-control',
                'placeholder':'Agrege numero de etiqueta',
                'id':'etiqueta'
                }),
            
            'secretaria':forms.Select(attrs={
                'class' :'form-control',
                'placeholder':'Agrege  secretaria',
                'id':'secretaria'
                }),
            'unidad':forms.Select(attrs={
                'class' :'form-control',
                'placeholder':'Agrege el unidad',
                'id':'unidad'
                }),
 
   

        
        }




#__________________________________________________-Unidad VERDADERO ________________________________
class UnidadvFormulario(forms.ModelForm):

    class Meta:
        model= Unidad
        fields=[
            'nombre',
            'sigla',
            'tipo',
            'detalle',
            'etiqueta',
            'secretaria',
     
  
        
           
        ]
        labels={

            'nombre':'Nombre',
            'sigla':'Sigla',
            'tipo':'Tipo',
            'detalle':'Direccion',
            'etiqueta':'Etiqueta',
            'secretaria':'Secretaria',
      
            

   
        }
        widgets={



            'nombre':forms.TextInput(attrs={
                'class' :'form-control',
                'placeholder':'Agrege numero de Red',
                'id':'nombre'
                }),
            'sigla':forms.TextInput(attrs={
                'class' :'form-control',
                'placeholder':'Agrege numero de sigla',
                'id':'sigla'
                }),
            'tipo':forms.TextInput(attrs={
                'class' :'form-control',
                'placeholder':'Agrege numero de tipo',
                'id':'tipo'
                }),
            'etiqueta':forms.TextInput(attrs={
                'class' :'form-control',
                'placeholder':'Agrege numero de etiqueta',
                'id':'etiqueta'
                }),
            
            'secretaria':forms.Select(attrs={
                'class' :'form-control',
                'placeholder':'Agrege  secretaria',
                'id':'secretaria'
                }),
            
            'detalle':forms.TextInput(attrs={
                'class' :'form-control',
                'placeholder':'Agrege Direccion',
                'id':'detalle'
                }),
         
 
   

        
        }





#__________________________________________________________-RELACION  SECRETARIA RED________________________________
class RedSecretariaFormulario(forms.ModelForm):

    class Meta:
        model= RedSecretaria
        fields=[
            'secretaria',
            'red',
   
        ]
        labels={

            'secretaria':'Secretaria',
            'red':'Red',
   
        }
        widgets={

            'secretaria':forms.Select(attrs={
                'class' :'form-control',
                'placeholder':'Agrege el secretaria',
                'id':'secretaria'
                }),
            'red':forms.Select(attrs={
                'class' :'form-control',
                'placeholder':'Agrege el red',
                'id':'red'
                }),

        }




#__________________________________________________________-RELACION  RED AREA ________________________________
class RedAreaFormulario(forms.ModelForm):

    class Meta:
        model= RedArea
        fields=[
            'area',
            'red',
   
        ]
        labels={

            'area':'Area',
            'red':'red',
   
        }
        widgets={

            'area':forms.Select(attrs={
                'class' :'form-control',
                'placeholder':'Agrege el area',
                'id':'area'
                }),
            'red':forms.Select(attrs={
                'class' :'form-control',
                'placeholder':'Agrege el red',
                'id':'red'
                }),

        }





#__________________________________________________________-RELACION  RED UNIDAD ________________________________
class RedUnidadFormulario(forms.ModelForm):

    class Meta:
        model= RedUnidad
        fields=[
            'unidad',
            'red',
   
        ]
        labels={

            'unidad':'Unidad',
            'red':'red',
   
        }
        widgets={

            'unidad':forms.Select(attrs={
                'class' :'form-control',
                'placeholder':'Agrege el unidad',
                'id':'unidad'
                }),
            'red':forms.Select(attrs={
                'class' :'form-control',
                'placeholder':'Agrege el red',
                'id':'red'
                }),

        }











































class SecretariaAreaFormulario(forms.ModelForm):

   class Meta:
        model= SecretariaArea
        fields=[
            'secretaria',
            'area',
     
           
        ]
        labels={

            'secretaria':'secretaria',
            'area':'area',
      
   
        }
        widgets={

            'secretaria':forms.Select(attrs={
                'class' :'form-control',
              

            }),
            'area':forms.Select(attrs={
                'class' :'form-control',
                'placeholder':'Agrege el area',
                'id':'area'
                })

        }


class SecretariaUnidadFormulario(forms.ModelForm):

   class Meta:
        model= SecretariaUnidad
        fields=[
            'secretaria',
            'unidad',
     
           
        ]
        labels={

            'secretaria':'secretaria',
            'unidad':'unidad',
      
   
        }
        widgets={

            'secretaria':forms.Select(attrs={
                'class' :'form-control',
              

            }),
            'unidad':forms.Select(attrs={
                'class' :'form-control',
                'placeholder':'Agrege el unidad',
                'id':'unidad'
                })

        }


























































class InfraestructuraSecretariaFormulario(forms.ModelForm):

   class Meta:
        model= InfraestructuraSecretaria
        fields=[
            'secretaria',
            'infraestructura',
     
           
        ]
        labels={

            'secretaria':'secretaria',
            'infraestructura':'infraestructura',
      
   
        }
        widgets={

            'secretaria':forms.Select(attrs={
                'class' :'form-control',
              

            }),
            'infraestructura':forms.Select(attrs={
                'class' :'form-control',
                'placeholder':'Agrege el infraestructura',
                'id':'infraestructura'
                })

        }

class InfraestructuraUnidadFormulario(forms.ModelForm):

   class Meta:
        model= InfraestructuraUnidad
        fields=[
            'unidad',
            'infraestructura',
     
           
        ]
        labels={

            'unidad':'unidad',
            'infraestructura':'infraestructura',
      
   
        }
        widgets={

            'unidad':forms.Select(attrs={
                'class' :'form-control',
              

            }),
            'infraestructura':forms.Select(attrs={
                'class' :'form-control',
                'placeholder':'Agrege el infraestructura',
                'id':'infraestructura'
                })

        }

class InfraestructuraAreaFormulario(forms.ModelForm):

   class Meta:
        model= InfraestructuraArea
        fields=[
            'area',
            'infraestructura',
     
           
        ]
        labels={

            'area':'unidad',
            'infraestructura':'infraestructura',
      
   
        }
        widgets={

            'area':forms.Select(attrs={
                'class' :'form-control',
              

            }),
            'infraestructura':forms.Select(attrs={
                'class' :'form-control',
                'placeholder':'Agrege el infraestructura',
                'id':'infraestructura'
                })

        }